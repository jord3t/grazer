//
//  UIViewExtensions.swift
//  ImageMap
//
//  Created by trevor jordet on 8/31/18.
//  Copyright © 2018 jord3t. All rights reserved.
//

extension UIView {
    func fadeIn(
        duration: TimeInterval = 1.0,
        delay: TimeInterval = 0.0,
        completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)
    }

    func fadeOut(
        duration: TimeInterval = 1.0,
        delay: TimeInterval = 0.0,
        originX: CGFloat = -500.00,
        completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration,
                       delay: delay,
                       options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.alpha = 0.0
            self.frame.origin.x = originX
            self.frame.origin.y = self.frame.origin.y
        }, completion: completion)
    }
}
