//
//  UtilityMethods.swift
//  ImageMap
//
//  Created by trevor jordet on 8/14/18.
//  Copyright © 2018 jord3t. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMobileAds
import Photos

class UtilityMethods {
    func getAppDelegate() -> AppDelegate {
        guard let appDelegate: AppDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                fatalError("Could not cast UIApplication.shared.delegate as AppDelegate")
        }
        return appDelegate
    }

    func saveCoreData(delegate: AppDelegate) {
        delegate.saveContext()
    }

    func addBannerViewToView(_ bannerView: GADBannerView, containerView: UIView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(bannerView)
        if #available(iOS 11.0, *) {
            // In iOS 11, we need to constrain the view to the safe area.
            positionBannerViewFullWidthAtBottomOfSafeArea(bannerView, containerView: containerView)
        } else {
            // In lower iOS versions, safe area is not available so we use
            // bottom layout guide and view edges.
            positionBannerViewFullWidthAtBottomOfView(bannerView, containerView: containerView)
        }
    }

    // MARK: - view positioning
    @available (iOS 11, *)
    func positionBannerViewFullWidthAtBottomOfSafeArea(_ bannerView: GADBannerView, containerView: UIView) {
        // Position the banner. Stick it to the bottom of the Safe Area.
        // Make it constrained to the edges of the safe area.
        let guide = containerView.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            guide.leftAnchor.constraint(equalTo: bannerView.leftAnchor),
            guide.rightAnchor.constraint(equalTo: bannerView.rightAnchor),
            guide.bottomAnchor.constraint(equalTo: bannerView.bottomAnchor)
            ])
    }

    func positionBannerViewFullWidthAtBottomOfView(_ bannerView: GADBannerView, containerView: UIView) {
        containerView.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .leading,
                                              relatedBy: .equal,
                                              toItem: containerView,
                                              attribute: .leading,
                                              multiplier: 1,
                                              constant: 0))
        containerView.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .trailing,
                                              relatedBy: .equal,
                                              toItem: containerView,
                                              attribute: .trailing,
                                              multiplier: 1,
                                              constant: 0))
        containerView.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .bottom,
                                              relatedBy: .equal,
                                              toItem: containerView,
                                              attribute: .bottom,
                                              multiplier: 1,
                                              constant: 0))
    }

    func resizeContainer(containerView: UIView, newHeight: CGFloat) {
        containerView.constraints.forEach { (constraint) in
            if constraint.firstAttribute == .height {
                constraint.constant = newHeight
            }
        }
    }

    func hintMe(targetView: UIViewController, constraint: NSLayoutConstraint, hintHeight: CGFloat) {
        let returnHeight: CGFloat = constraint.constant
        constraint.constant = hintHeight
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            targetView.view.layoutIfNeeded()
        }, completion: {(_: Bool) -> Void in
            constraint.constant = returnHeight
            UIView.animate(withDuration: 0.2, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                targetView.view.layoutIfNeeded()
            })
        })
    }

    func resizeImageView(containerView: UIImageView, newWidth: CGFloat, newHeight: CGFloat) {
        containerView.constraints.forEach { (constraint) in
            if constraint.firstAttribute == .height {
                constraint.constant = newHeight
            }
            if constraint.firstAttribute == .width {
                constraint.constant = newWidth
            }
        }
    }
}
