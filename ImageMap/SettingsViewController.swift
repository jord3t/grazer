//
//  SettingsViewController.swift
//  ImageMap
//
//  Created by trevor jordet on 8/30/18.
//  Copyright © 2018 jord3t. All rights reserved.
//

import UIKit
import MobileCoreServices

class SettingsViewController: UIViewController, UIDocumentPickerDelegate {

    let documentInteractionController = UIDocumentInteractionController()

    @IBOutlet weak var backupButton: UIButton!
    @IBOutlet weak var restoreButton: UIButton!
    @IBOutlet weak var removeAdsButton: UIButton!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var licencingText: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let appVersionString = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString")
        let buildNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion")!
        // swiftlint:disable line_length
        let versionAndBuildNumber: String = "GeoPic version \(appVersionString!).\(buildNumber)"
        self.versionLabel.text = versionAndBuildNumber
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipe))
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        self.view.addGestureRecognizer(swipeDown)
        var detailsString: String = "Instructions:"
        detailsString += "\nTap thumbnail in map bubble to view Image full screen."
        detailsString += "\nSingle Tap Image to show/hide controls and description."
        detailsString += "\nDouble Tap Image to toggle fit or full screen."
        detailsString += "\nSwipe up/down title/description to show/hide description."
        detailsString += "\nPinch and drag to zoom/pan the image."
        detailsString += "\nSwipe left/right on image to advance to the next/previous image."
        detailsString += "\nSwipe down on area outside of minimized image to exit detail/edit screen."
        detailsString += "\nLong press for 2 seconds on map to move last selected image to the coordinates of the long press location."
        detailsString += "\nThe thumbnail selector view will appear at top of screen for images that are clustered close together. Tap thumbnails to hilight corresponding pin on map."
        detailsString += "\nDouble-tap the map to zoon in."
        detailsString += "\nTap the map with 2 fingers to zoom out."

        detailsString += "\n\nBottom buttons:\nprevious image, share image, refresh map/images, take a picture, navigate to location of the current imagein your favorite GPS app, and next image."

        detailsString += "\n\nTools Panel:\nThe radius slider controls the radius in meters surrounding the selected location. This is represented by a green circle on the map. Any photos within this radius will be added or removed from the targeted album."
        detailsString += "\nThe text entry field can be edited manually, or populated by selecting an album name from the picker below it. If a new name is entered, a new album will be created when Add Photo(s) button is tapped."
        detailsString += "\nAll images within the green circle radius on the map will be added to the album selected in the tool panel text field when the Add Photo(s) button is tapped."
        detailsString += "\nAll images within the green circle radius on the map will be removed from the selected album when the Remove Photo(s) button is tapped."

        detailsString += "\n\nSettings Screen:\nTap the Backup Map Data button/Save to Files to save your image title/description and GPS coordinates in a text file that you can later reload by tapping the Load Map Data From Backup button if you ever delete and reinstall GeoPic. MAKE SURE TO SAVE THIS IN A DIFFERENT DOCUMENT LOCATION OTHER THAN THE GeoPic FOLDER!."
        detailsString += "\n\nYou can remove Ads for $.99 by tapping the Remove Ads button. If you delete and reinstall GeoPic, you will not be charged a second time if you tap this button to remove ads on the new install."

        detailsString += "\n\nLicences"
        detailsString += "\nSwiftyJSON: The MIT License (MIT)"
        detailsString += "\nTPKeyboardAvoiding: zlib License"
        detailsString += "\nFabric: https://fabric.io/terms/fabric#licences-rights-restrictions"
        detailsString += "\nIcon Images: Creative Commons"
        detailsString += "\n\nGeoPic does not collect any personal data or photos from any users. Your data is private! Anonymous crashalytics data is monitored for the sole purpose of fixing any bugs in the code."
        self.licencingText.text = detailsString
        self.backupButton.layer.cornerRadius = 8.0
        self.restoreButton.layer.cornerRadius = 8.0
        self.removeAdsButton.layer.cornerRadius = 8.0
    }

    @IBAction func backupData(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.share(url: ImageDetail.convertRecordsToJSON(
                context: UtilityMethods().getAppDelegate().persistentContainer.viewContext))
        }
    }

    func share(url: URL) {
        documentInteractionController.url = url
        documentInteractionController.uti = String(kUTTypePlainText)
        documentInteractionController.name = url.lastPathComponent
        documentInteractionController.presentOptionsMenu(from: CGRect(x: 0, y: 0, width: 200.0, height: 200.0), in: self.view, animated: true)
    }

    @IBAction func loadBackupData(_ sender: UIButton) {
        let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePlainText)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .fullScreen
        self.present(importMenu, animated: true, completion: nil)
    }

    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        var returnJSON: JSON = JSON()
        let myURL = url as URL
        do {
            let data = try Data(contentsOf: myURL, options: .alwaysMapped)
            let json = try? JSON(data: data)
            if json != nil {
                returnJSON = json!
            }
        } catch {}
        ImageDetail.convertBackupToJSON(json: returnJSON,
                                        context: UtilityMethods().getAppDelegate().persistentContainer.viewContext)
    }

    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func RemoveAdsButtonTapped(_ sender: UIButton) {
        let removeAdsViewController = InAppPurchaseViewController()
        self.present(removeAdsViewController, animated: true, completion: nil)
    }

    @objc func respondToSwipe(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.down:
                self.dismiss(animated: true, completion: nil)
            default: break
            }
        }
    }
}
