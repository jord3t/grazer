//
//  InAppPurchaseModel.swift
//  ImageMap
//
//  Created by trevor jordet on 8/25/18.
//  Copyright © 2018 jord3t. All rights reserved.
//

import StoreKit
enum PurchaseState: String {
    typealias RawValue = String
    case notPurchased = "Show Ads"
    case purchased = "Remove Ads"
    case notAvailable = "Remove Ads Not Available"
}
class InAppPurchaseModel {
    var products: [SKProduct] = []
    var purchaseState: PurchaseState?
    var product: SKProduct? {
        didSet {
            guard product != nil else { return }
        }
    }

    @objc func setPurchasedState() {
        GeoPicProducts.store.requestProducts { success, products in
            if success {
                self.products = products!
                self.product = self.products[0]
                if GeoPicProducts.store.isProductPurchased((self.product?.productIdentifier)!) {
                    self.purchaseState = .purchased
                } else if IAPHelper.canMakePayments() {
                        self.purchaseState = .notPurchased
                } else {
                    self.purchaseState = .notAvailable
                }
            } else {
                self.purchaseState = .notAvailable
            }
            NotificationCenter.default.post(
                name: Notification.Name.NotificationType.inAppPurchaseStateSet,
                object: nil,
                userInfo: nil)
        }
    }
}
