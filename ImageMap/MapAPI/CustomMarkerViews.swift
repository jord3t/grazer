import MapKit

class CustomMarkerViews: MKMarkerAnnotationView {
    override var annotation: MKAnnotation? {
        willSet {
            guard let mapPin = newValue as? CustomMapPin else { return }
            canShowCallout = true
            calloutOffset = CGPoint(x: -5, y: 5)
            rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            markerTintColor = mapPin.markerTintColor
        }
    }
}

class ImageMarkerView: MKAnnotationView {
    var index: Int!
    override var annotation: MKAnnotation? {
        willSet {
            guard let mapPin = newValue as? CustomMapPin else {return}
            self.index = mapPin.index
            canShowCallout = true
            calloutOffset = CGPoint(x: 0, y: 0)
            let mapsButton = UIButton(frame: CGRect(origin: CGPoint.zero,
                                                    size: CGSize(width: 80, height: 80)))
            mapsButton.setBackgroundImage(mapPin.image, for: UIControl.State())
            mapsButton.layer.cornerRadius = 5.0
            mapsButton.layer.masksToBounds = true
            rightCalloutAccessoryView = mapsButton
            let imageView = UIImageView(frame: CGRect(origin: CGPoint.zero,
                                                      size: CGSize(width: 30, height: 30)))
            imageView.contentMode = .scaleAspectFit
            imageView.image = mapPin.image
            imageView.layer.cornerRadius = imageView.layer.frame.size.width / 5
            imageView.layer.masksToBounds = true
            self.addSubview(imageView)
            self.frame = imageView.frame
            let detailLabel = UILabel()
            detailLabel.numberOfLines = 0
            detailLabel.font = detailLabel.font.withSize(12)
            detailLabel.text = mapPin.subtitle!
            self.detailCalloutAccessoryView = detailLabel
        }
    }
}
