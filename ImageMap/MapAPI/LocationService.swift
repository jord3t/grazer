//
//  LocationServices.swift
//  ImageMap
//
//  Created by trevor jordet on 8/13/18.
//  Copyright © 2018 jord3t. All rights reserved.
//

import Foundation
import MapKit

typealias JSONDictionary = [String: Any]

class LocationService {
    let inUse = CLAuthorizationStatus.authorizedWhenInUse
    let always = CLAuthorizationStatus.authorizedAlways
    func getAddress(location: CLLocation, completion: @escaping (_ address: CLPlacemark?, _ error: Error?) -> Void) {
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location) { placemarks, error in
            if let e = error {
                completion(nil, e)
            } else {
                let placeArray: [CLPlacemark] = placemarks!
                let address: CLPlacemark = placeArray[0]
                completion(address, nil)
                NotificationCenter.default.post(
                    name: Notification.Name.NotificationType.refreshImageData,
                    object: nil,
                    userInfo: nil)
            }
        }
    }
}
