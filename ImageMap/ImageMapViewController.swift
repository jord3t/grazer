//
//  HomeViewController.swift
//  ImageMap
//
//  Created by trevor jordet on 8/8/18.
//  Copyright © 2018 jord3t. All rights reserved.
//

import UIKit
import MapKit
import Photos
import Contacts
//swiftlint:disable:next type_body_length
class ImageMapViewController: BaseViewController {
    var imageArray = [PHAsset]()
    var imageData: ImageData!
    var sortedAnnotations: [CustomMapPin] = []
    let navVC = BottomNavViewController()
    var longGesture: UILongPressGestureRecognizer!
    var panGesture: UIPanGestureRecognizer!
    var longGestureStarted: Bool = false
    var moveTitle: String!
    var moveDescription: String!
    var regionRadius: Double = 800.0
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var bottomNavView: UIView!
    @IBOutlet weak var albumSelectTextField: UITextField!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var toolPanelView: UIView!
    @IBOutlet weak var dragToolPanelView: UIView!
    @IBOutlet weak var toolPickerView: UIPickerView!
    @IBOutlet weak var toolTextField: UITextField!
    @IBOutlet weak var toolSliderView: UISlider!
    @IBOutlet weak var radiusTextField: UILabel!
    @IBOutlet weak var multiplePinCollectionView: UICollectionView!
    @IBOutlet weak var removePhotoAlbumButton: UIButton!

    var multiplePinArray: [CustomMapPin] = []
    var cellId = "Cell"
    var indexPath: IndexPath!
    var selectedAlbum: String = "All"
    var assetCollection: PHAssetCollection!
    var albumFound: Bool = false
    var collection: PHAssetCollection!
    var assetCollectionPlaceholder: PHObjectPlaceholder!
    var circleRadius: Double = 0.1
    var singleTapGesture: UITapGestureRecognizer!
    var doubleTapGesture: UITapGestureRecognizer!
    var circle: MKCircle!
    var centralLocationCoordinate: CLLocation = CLLocation()
    var activeCoordinate: CLLocation!
    var showToolsButton: UIBarButtonItem!

    override init() {
        super.init()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.showNextPin),
                                               name: NSNotification.Name.NotificationType.loadNextImage,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.showPreviousPin),
                                               name: NSNotification.Name.NotificationType.loadPreviousImage,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reloadMap),
                                               name: NSNotification.Name.NotificationType.refreshImageData,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.processImageArray),
                                               name: NSNotification.Name.NotificationType.processImages,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.pinCollectionCellEvent),
                                               name: NSNotification.Name.NotificationType.selectPinOrCellEvent,
                                               object: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if UtilityMethods().getAppDelegate().inAppPurchaseModel.purchaseState == .purchased {
            UtilityMethods().resizeContainer(containerView: self.bottomNavView, newHeight: 50.0)
        }
        self.addChild(navVC)
        navVC.view.frame = self.bottomNavView.bounds
        self.bottomNavView.addSubview(navVC.view)
        mapView.register(ImageMarkerView.self,
                         forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        self.mapView.delegate = self
        self.longGesture = UILongPressGestureRecognizer(target: self, action: #selector(movePin(longGesture:)))
        self.longGesture.minimumPressDuration = 2.0
        self.mapView.addGestureRecognizer(self.longGesture)
        self.loadAlbum()
        self.multiplePinCollectionView.allowsMultipleSelection = false
        self.multiplePinCollectionView.dataSource = self
        self.multiplePinCollectionView.delegate = self
        self.multiplePinCollectionView.register(MarkerAnnotationCell.self, forCellWithReuseIdentifier: cellId)
        self.imageData = UtilityMethods().getAppDelegate().imageDataModel
        self.albumSelectTextField.delegate = self
        self.pickerView.delegate = self
        self.doubleTapGesture = UITapGestureRecognizer(target: self,
                                                       action: #selector(doubleTapMethod(tapGesture:)))
        self.singleTapGesture = UITapGestureRecognizer(target: self,
                                                          action: #selector(addoverlaycircleToMap(tapGesture:)))
        self.doubleTapGesture.numberOfTapsRequired = 2
        self.singleTapGesture.numberOfTapsRequired = 1
        self.singleTapGesture.require(toFail: self.doubleTapGesture)
        self.mapView.addGestureRecognizer(self.singleTapGesture)
        self.mapView.addGestureRecognizer(self.doubleTapGesture)
        self.setButtonBarButtons()
        self.addToolPanel()
    }

    func addToolPanel() {
        self.toolPanelView.layer.borderWidth = 2.0
        self.toolPanelView.layer.borderColor = UIColor.init(red: 78/255,
                                                            green: 157/255,
                                                            blue: 198/255,
                                                            alpha: 1.0).cgColor
        self.toolPanelView.layer.cornerRadius = 10.0
        self.toolPanelView.layer.masksToBounds = true
        self.panGesture = UIPanGestureRecognizer(target: self,
                                                 action: #selector(moveToolPanel(panGesture:)))
        self.dragToolPanelView.addGestureRecognizer(self.panGesture)
        self.toolPickerView.delegate = self
    }

    func setButtonBarButtons() {
        self.showToolsButton = UIBarButtonItem(title: "Show Tools",
                                               style: UIBarButtonItem.Style.plain,
                                               target: self, action: #selector(self.showHideTools))
        let settingsButton: UIButton = UIButton(type: UIButton.ButtonType.custom)
        settingsButton.setImage(UIImage(named: "settings.png"), for: UIControl.State.normal)
        settingsButton.addTarget(self,
                                 action: #selector(ImageMapViewController.settingsButtonTapped),
                                 for: UIControl.Event.touchUpInside)
        settingsButton.widthAnchor.constraint(equalToConstant: 40).isActive = true
        settingsButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        let barButton = UIBarButtonItem(customView: settingsButton)
        self.navigationItem.setRightBarButtonItems([self.showToolsButton, barButton], animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UtilityMethods().getAppDelegate().imageDataModel.assetArray.count > 0 && self.sortedAnnotations.count > 0 {
            self.selectMapPin()
        }
    }

    @objc func settingsButtonTapped() {
        let settingsViewController = SettingsViewController()
        self.present(settingsViewController, animated: true, completion: nil)
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func loadAlbum() {
        self.mapView.removeAnnotations(self.mapView.annotations)
        if self.selectedAlbum == "All" {
            UtilityMethods().getAppDelegate().imageDataModel.setAssetArray()
        } else {
            UtilityMethods().getAppDelegate().imageDataModel.fetchCustomAlbumPhotos(albumName: self.selectedAlbum)
        }
    }

    @IBAction func toolAddImagesToAlbum(_ sender: UIButton) {
       self.createAlbum(title: self.toolTextField.text!)
    }

    @IBAction func toolRemoveImagesFromAlbum(_ sender: UIButton) {
        self.removeAssetsFromAlbum(title: self.toolTextField.text!)
    }

    func createAlbum(title: String) {
        if title == "All" { return }
        //Get PHFetch Options
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", title)
        let collection: PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album,
                                                                                subtype: .any,
                                                                                options: fetchOptions)
        //Check return value - If found, then get the first album out
        if let _: AnyObject = collection.firstObject {
            self.albumFound = true
            self.assetCollection = collection.firstObject
            for i in 0..<self.multiplePinArray.count {
                self.addImageToAlbum(photosAsset: self.imageData.assetArray[self.multiplePinArray[i].index])
            }
            DispatchQueue.main.async {
                if self.multiplePinArray.count > 0 {
                    self.showAlert(title: "Images Added",
                                   message: "Added \(self.multiplePinArray.count) images to \(title)")
                }
            }
        } else {
            //If not found - Then create a new album
            PHPhotoLibrary.shared().performChanges({
                //swiftlint:disable next: line_length
                let createAlbumRequest: PHAssetCollectionChangeRequest = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(
                    withTitle: title)
                self.assetCollectionPlaceholder = createAlbumRequest.placeholderForCreatedAssetCollection
            }, completionHandler: { success, _ in
                self.albumFound = success
                if success {
                    let collectionFetchResult = PHAssetCollection.fetchAssetCollections(
                        withLocalIdentifiers: [self.assetCollectionPlaceholder.localIdentifier],
                        options: nil)
                    self.assetCollection = collectionFetchResult.firstObject
                    for i in 0..<self.multiplePinArray.count {
                        self.addImageToAlbum(photosAsset: self.imageData.assetArray[self.multiplePinArray[i].index])
                    }
                    DispatchQueue.main.async {
                        if self.multiplePinArray.count > 0 {
                            self.showAlert(title: "New Album: \(title) Created",
                                message: "Added \(self.multiplePinArray.count) images to \(title)")
                        }
                        self.pickerView.reloadAllComponents()
                        self.toolPickerView.reloadAllComponents()
                    }
                }
            })
        }
    }

    func addImageToAlbum(photosAsset: PHAsset) {
        PHPhotoLibrary.shared().performChanges({
            let photosAssets = PHAsset.fetchAssets(in: self.assetCollection, options: nil)
            let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection, assets: photosAssets)!
            albumChangeRequest.addAssets([photosAsset] as NSFastEnumeration)
        }, completionHandler: { _, _ in
             print("added image to album")
        })
    }

    func removeAssetsFromAlbum(title: String) {
        //Get PHFetch Options
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", title)
        let collection: PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album,
                                                                                subtype: .any,
                                                                                options: fetchOptions)
        //Check return value - If found, then get the first album out
        if let _: AnyObject = collection.firstObject {
            self.albumFound = true
            self.assetCollection = collection.firstObject
            if self.multiplePinArray.count > 1 {
                for i in 0..<self.multiplePinArray.count {
                    self.removeImageFromAlbum(photosAsset: self.imageData.assetArray[self.multiplePinArray[i].index])
                }
                if self.multiplePinArray.count > 0 {
                    self.showAlert(title: "Images Removed", message: "Removed \(self.multiplePinArray.count) images from \(title)")
                }
            } else {
                if self.imageData.assetArray.count > 0 {
                    self.removeImageFromAlbum(photosAsset: self.imageData.assetArray[self.imageData.selectedPin])
                }
                if self.multiplePinArray.count > 0 {
                    self.showAlert(title: "Image Removed", message: "Removed image from \(title)")
                }
            }
            self.loadAlbum()
            self.setToolsRemoveButtonState()
            sleep(1)
            if self.checkForEmptyAlbum() {
                PHPhotoLibrary.shared().performChanges({
                    PHAssetCollectionChangeRequest.deleteAssetCollections([self.assetCollection] as NSFastEnumeration)
                }, completionHandler: { (success, error) in
                    if success {
                        DispatchQueue.main.async {
                            self.selectedAlbum = "All"
                            self.albumSelectTextField.text = self.selectedAlbum
                            self.toolTextField.text = ""
                            self.loadAlbum()
                            self.pickerView.reloadAllComponents()
                            self.toolPickerView.reloadAllComponents()
                            self.removePhotoAlbumButton.setTitle("Remove Photo(s)", for: .normal)
                        }
                    } else if let error = error {
                        print(error.localizedDescription)
                    }
                })
            }
        }
    }

    func removeImageFromAlbum(photosAsset: PHAsset) {
        PHPhotoLibrary.shared().performChanges({
            let photosAssets = PHAsset.fetchAssets(in: self.assetCollection, options: nil)
            let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection, assets: photosAssets)!
            albumChangeRequest.removeAssets([photosAsset] as NSFastEnumeration)
        }, completionHandler: { _, _ in
            print("removed image from album")
        })
    }

    func checkForEmptyAlbum() -> Bool {
        if self.imageData?.assetArray.count == 0 {
            self.mapView.removeAnnotations(self.mapView.annotations)
            return true
        }
        return false
    }

    @objc func showNextPin(notification: Notification) {
        if self.checkForEmptyAlbum() { return }
        if self.imageData.selectedPin < self.imageData.assetArray.count - 1 {
            self.imageData.selectedPin += 1
        } else {
            self.imageData.selectedPin = 0
        }
        let selectedAnnotation: MKAnnotation = self.sortedAnnotations[self.imageData.selectedPin]
        let coordinate: CLLocation = CLLocation(latitude: selectedAnnotation.coordinate.latitude,
                                                longitude: selectedAnnotation.coordinate.longitude)
        self.mapView.selectAnnotation(selectedAnnotation, animated: true)
        self.populateCollectionView(location: coordinate)
        if self.imageData.assetArray.count >= self.imageData.selectedPin {
            self.storeImageDetailRecord(phAsset: UtilityMethods().getAppDelegate().imageDataModel.selectedAsset!)
        }
        self.drawCircle(location: coordinate)
    }

    @objc func reloadMap(notification: Notification) {
        self.loadAlbum()
    }

    @objc func showPreviousPin(notification: Notification) {
        if self.checkForEmptyAlbum() { return }
        if self.imageData.selectedPin > 0 {
            self.imageData.selectedPin -= 1
        } else {
            self.imageData.selectedPin = self.imageData.assetArray.count - 1
        }
        let selectedAnnotation: MKAnnotation = self.sortedAnnotations[self.imageData.selectedPin]
        let coordinate: CLLocation = CLLocation(latitude: selectedAnnotation.coordinate.latitude,
                                                longitude: selectedAnnotation.coordinate.longitude)
        self.mapView.selectAnnotation(selectedAnnotation, animated: true)
        self.populateCollectionView(location: coordinate)
        if self.imageData.assetArray.count >= self.imageData.selectedPin {
            self.storeImageDetailRecord(phAsset: self.imageData.selectedAsset!)
        }
        self.drawCircle(location: coordinate)
    }

    func storeImageDetailRecord(phAsset: PHAsset) {
        let imageRecord: ImageDetail = ImageDetail.getRecordByID(
            identifier: phAsset.localIdentifier,
            context: UtilityMethods().getAppDelegate().persistentContainer.viewContext)
        if !imageRecord.addressLookupSuccessful {
            LocationService().getAddress(location: CLLocation(
                latitude: imageRecord.latCoord,
                longitude: imageRecord.lonCoord)) { address, _ in
                    var detailText: String = "\((phAsset.modificationDate?.toString())!)"
                    if let addressName: String = address?.name {
                        detailText += "\n\(addressName)"
                    }
                    if let city: String = address?.locality {
                        detailText += "\n\(city)"
                        imageRecord.titleString = city
                    }
                    if let state: String = address?.administrativeArea {
                        detailText += ", \(state)"
                    }
                    if let postalCode: String = address?.postalCode {
                        detailText += " \(postalCode)"
                    }
                    imageRecord.addressLookupSuccessful = true
                    imageRecord.descriptionString = detailText
                    UtilityMethods().saveCoreData(delegate: UtilityMethods().getAppDelegate())
            }
        }
    }

    func centerMapOnLocation(location: CLLocation, regionRadius: CLLocationDistance, animated: Bool) {
        let coordinateRegion = MKCoordinateRegion.init(center: location.coordinate,
                                                       latitudinalMeters: regionRadius,
                                                       longitudinalMeters: regionRadius)
        self.mapView.setRegion(coordinateRegion, animated: animated)
    }

    @objc func processImageArray(notification: Notification) {
        guard let assetArray = notification.object as? [PHAsset] else {
            fatalError("could not cast notification.object as [PHAsset]")
        }
        UtilityMethods().getAppDelegate().imageDataModel.assetArray = assetArray
        self.sortedAnnotations.removeAll()
        mapView.removeAnnotations(mapView.annotations)
        for i in 0..<UtilityMethods().getAppDelegate().imageDataModel.assetArray.count {
            let imageAsset: PHAsset = UtilityMethods().getAppDelegate().imageDataModel.assetArray[i]
            var locationNameString: String
            let imageRecord: ImageDetail! = ImageDetail.getRecordByID(
                identifier: imageAsset.localIdentifier,
                context: UtilityMethods().getAppDelegate().persistentContainer.viewContext)
            if imageRecord.addressLookupSuccessful {
                locationNameString = imageRecord.descriptionString!
            } else {
                locationNameString = "\((imageAsset.modificationDate?.toString())!)"
            }
            let detailedDescription = imageRecord.detailedDescription
            if detailedDescription != nil {
                locationNameString += "\n\(imageRecord.detailedDescription!)"
            }
            let thumbImage = imageAsset.getUIResizedImage(width: 100, height: 100, contentMode: .aspectFill)
            var coordinate: CLLocationCoordinate2D? = imageAsset.location?.coordinate
            if imageRecord.latCoord != 0.0 && imageRecord.lonCoord != 0.0 {
                coordinate = CLLocationCoordinate2D(latitude: imageRecord.latCoord, longitude: imageRecord.lonCoord)
            } else {
                imageRecord.latCoord = (coordinate?.latitude)!
                imageRecord.lonCoord = (coordinate?.longitude)!
            }
            let mapPin = CustomMapPin(title: imageRecord.titleString ?? "image \(i)",
                locationName: locationNameString, coordinate: coordinate!, image: thumbImage,
                localIdentifier: imageAsset.localIdentifier, index: i)
            mapView.addAnnotation(mapPin)
            self.sortedAnnotations.append(mapPin)
        }
        UtilityMethods().saveCoreData(delegate: UtilityMethods().getAppDelegate())
        if UtilityMethods().getAppDelegate().imageDataModel.assetArray.count > 0 {
            NotificationCenter.default.post(name: Notification.Name.NotificationType.activateNavButtons,
                                            object: nil, userInfo: nil)
            self.selectMapPin()
        } else {
            NotificationCenter.default.post(name: Notification.Name.NotificationType.deactivateNavButtons,
                                            object: nil, userInfo: nil)
        }
    }

    func populateMovedPin(notification: Notification) {
        let imageAsset: PHAsset = self.imageData.assetArray[self.imageData.selectedPin]
        let imageRecord = ImageDetail.getRecordByID(
            identifier: imageAsset.localIdentifier,
            context: UtilityMethods().getAppDelegate().persistentContainer.viewContext)
        imageRecord.titleString = self.moveTitle
        imageRecord.detailedDescription = self.moveDescription
        UtilityMethods().saveCoreData(delegate: UtilityMethods().getAppDelegate())
        self.processImageArray(notification: notification)
    }

    @IBAction func hideToolPanel(_ sender: UIButton) {
        self.toolPanelView.isHidden = true
        self.showToolsButton.title = "Show Tools"
    }

    @objc func showHideTools() {
        if self.toolPanelView.isHidden {
            self.toolPanelView.frame.origin.x = self.view.frame.width - (self.toolPanelView.frame.width + 10)
            self.toolPanelView.frame.origin.y = 100
            self.toolPanelView.isHidden = false
            self.showToolsButton.title = "Hide Tools"
        } else {
            self.toolPanelView.isHidden = true
            self.showToolsButton.title = "Show Tools"
        }
    }

    @IBAction func centerOnMyLocation(_ sender: UIButton) {
        if locationManager.location != nil {
            self.centerMapOnLocation(location: locationManager.location!,
                                     regionRadius: self.regionRadius,
                                     animated: true)
        }
    }

    @objc func populateCollectionView(location: CLLocation) {
        self.indexPath = nil
        self.multiplePinArray = []
        for i in 0..<self.sortedAnnotations.count {
            if CLLocation(latitude: self.sortedAnnotations[i].coordinate.latitude,
                          longitude: self.sortedAnnotations[i].coordinate.longitude).distance(from: location) <= CLLocationDistance(self.circleRadius * self.getRadius()) {
                self.multiplePinArray.append(self.sortedAnnotations[i])
            }
        }
        if self.multiplePinArray.count > 1 {
            self.multiplePinCollectionView.isHidden = false
            self.multiplePinCollectionView.reloadData()
        } else {
            self.multiplePinCollectionView.reloadData()
            self.multiplePinCollectionView.isHidden = true
        }
        for i in 0..<self.multiplePinArray.count where self.multiplePinArray[i].index == self.imageData.selectedPin {
            self.indexPath = IndexPath(row: i, section: 0)
        }

        NotificationCenter.default.post(name: Notification.Name.NotificationType.selectPinOrCellEvent,
                                        object: nil,
                                        userInfo: nil)
        if indexPath != nil {
            self.multiplePinCollectionView.selectItem(at: self.indexPath,
                                                      animated: false,
                                                      scrollPosition: .centeredHorizontally)
            self.multiplePinCollectionView.delegate?.collectionView!(multiplePinCollectionView,
                                                                     didSelectItemAt: self.indexPath)
        }
    }

    @objc func selectMapPin() {
        var coordinate: CLLocation!
        if self.checkForEmptyAlbum() { return }
        if self.imageData.selectedPin == -1 {
            self.imageData.selectedPin = self.imageData.assetArray.count - 1
            let selectedAnnotation: MKAnnotation = self.sortedAnnotations[self.imageData.selectedPin]
            coordinate = CLLocation(latitude: selectedAnnotation.coordinate.latitude,
                                                    longitude: selectedAnnotation.coordinate.longitude)
            self.centerMapOnLocation(location: coordinate, regionRadius: self.regionRadius, animated: false)
            self.mapView.selectAnnotation(selectedAnnotation, animated: false)
        } else {
            if self.imageData.selectedPin > self.imageData.assetArray.count - 1 {
                self.imageData.selectedPin = self.imageData.assetArray.count - 1
            }
            let selectedAnnotation: MKAnnotation = self.sortedAnnotations[self.imageData.selectedPin]
            self.mapView.selectAnnotation(selectedAnnotation, animated: true)
            coordinate = CLLocation(latitude: selectedAnnotation.coordinate.latitude,
                                                    longitude: selectedAnnotation.coordinate.longitude)
        }
        self.activeCoordinate = coordinate
        self.drawCircle(location: self.activeCoordinate)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.longGestureStarted = true
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.longGestureStarted = false
    }

    @objc func pinCollectionCellEvent(notification: Notification) {
        if notification.object != nil {
            self.selectMapPin()
        } else {
            if self.indexPath != nil {
                self.multiplePinCollectionView.scrollToItem(at: self.indexPath,
                                                            at: [.centeredHorizontally],
                                                            animated: true)
            }
        }
    }

    @objc func moveToolPanel(panGesture: UIGestureRecognizer) {
        let touchPoint: CGPoint = panGesture.location(in: self.view)
        self.toolPanelView.frame.origin.x = touchPoint.x - self.toolPanelView.frame.size.width/2
        self.toolPanelView.frame.origin.y = touchPoint.y - 20.0
    }

    @objc func movePin(longGesture: UIGestureRecognizer) {
        if !self.longGestureStarted {
            return
        }
        self.longGestureStarted = false
        let touchPoint = longGesture.location(in: mapView)
        let wayCoords = mapView.convert(touchPoint, toCoordinateFrom: mapView)
        let location = CLLocation(latitude: wayCoords.latitude, longitude: wayCoords.longitude)
        let selectedAnnotation: MKAnnotation = self.sortedAnnotations[self.imageData.selectedPin]
        self.mapView.deselectAnnotation(selectedAnnotation, animated: false)
        self.mapView.removeAnnotation(selectedAnnotation)
        let imageAsset: PHAsset = self.imageData.assetArray[self.imageData.selectedPin]
        let imageRecord = ImageDetail.getRecordByID(
            identifier: imageAsset.localIdentifier,
            context: UtilityMethods().getAppDelegate().persistentContainer.viewContext)
        let mapPin = CustomMapPin(title: imageRecord.titleString ?? "",
            locationName: "" ,
            coordinate: location.coordinate,
            image: imageAsset.getUIResizedImage(width: 100, height: 100, contentMode: .aspectFill),
            localIdentifier: imageAsset.localIdentifier,
            index: self.imageData.selectedPin)
        self.mapView.addAnnotation(mapPin)
        self.sortedAnnotations[self.imageData.selectedPin] = mapPin
        imageRecord.latCoord = location.coordinate.latitude
        imageRecord.lonCoord = location.coordinate.longitude
        imageRecord.addressLookupSuccessful = false
        self.moveTitle = imageRecord.titleString ?? ""
        self.moveDescription = imageRecord.detailedDescription ?? ""
        UtilityMethods().saveCoreData(delegate: UtilityMethods().getAppDelegate())
        NotificationCenter.default.post(
            name: Notification.Name.NotificationType.processImages,
            object: self.imageData.assetArray,
            userInfo: nil)
        self.storeImageDetailRecord(phAsset: imageAsset)
    }

    @objc func doubleTapMethod(tapGesture: UIGestureRecognizer) {
        //dummy method needed to cancel out singleTap
    }

    @objc func addoverlaycircleToMap(tapGesture: UIGestureRecognizer) {
        let touchPoint = tapGesture.location(in: mapView)
        let wayCoords = mapView.convert(touchPoint, toCoordinateFrom: mapView)
        let location = CLLocation(latitude: wayCoords.latitude, longitude: wayCoords.longitude)
        self.drawCircle(location: location)
    }

    func drawCircle(location: CLLocation) {
        if self.toolPickerView.isHidden { return }
        if self.circle != nil {
            self.mapView.removeOverlay(self.circle)
        }
        self.circle = MKCircle(center: location.coordinate,
                               radius: CLLocationDistance(self.circleRadius * self.getRadius()))
        self.mapView.addOverlay(self.circle)
        self.multiplePinCollectionView.reloadData()
        self.populateCollectionView(location: location)
        self.activeCoordinate = location
    }

    @IBAction func setCircleRadius(_ sender: UISlider) {
        self.toolTextField.endEditing(true)
        self.circleRadius = Double(sender.value)
        self.radiusTextField.text =  "\(round(self.getRadius()/5 * Double(sender.value * 10))) M"
        self.drawCircle(location: self.activeCoordinate)
    }

    deinit {
        UtilityMethods().getAppDelegate().imageDataModel.selectedPin = -1
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.NotificationType.loadNextImage,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.NotificationType.loadPreviousImage,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.NotificationType.refreshImageData,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.NotificationType.processImages,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.NotificationType.selectPinOrCellEvent,
                                                  object: nil)
    }
}

extension ImageMapViewController: UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout,
    UIPickerViewDelegate,
    UIPickerViewDataSource,
    UITextFieldDelegate {
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.imageData.getAlbumTitles().count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.imageData.getAlbumTitles()[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView {
        case self.pickerView:
            self.imageData.selectedPin = 0
            self.selectedAlbum = self.imageData.getAlbumTitles()[row]
            self.albumSelectTextField.text = "Album: \(self.selectedAlbum)"
            self.pickerView.isHidden = true
            self.multiplePinCollectionView.isHidden = true
            self.multiplePinArray.removeAll()
            self.loadAlbum()
        default:
            self.toolTextField.text = self.imageData.getAlbumTitles()[row]
            self.setToolsRemoveButtonState()
        }
        self.view.endEditing(true)
    }

    func setToolsRemoveButtonState() {
        if self.toolTextField.text! == "All" {
            self.removePhotoAlbumButton.setTitle("Remove Photo(s)", for: .normal)
        }
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", self.toolTextField.text!)
        let collection: PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album,
                                                                                subtype: .any,
                                                                                options: fetchOptions)
        //Check return value - If found, then get the first album out
        if let _: AnyObject = collection.firstObject {
            self.albumFound = true
            self.assetCollection = collection.firstObject
            if self.assetCollection.estimatedAssetCount == 0 {
                self.removePhotoAlbumButton.setTitle("Remove Album", for: .normal)
            } else {
                self.removePhotoAlbumButton.setTitle("Remove Photo(s)", for: .normal)
            }
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.albumSelectTextField {
            self.pickerView.isHidden = false
            textField.endEditing(true)
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.multiplePinArray.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = self.multiplePinCollectionView.dequeueReusableCell(
            withReuseIdentifier: cellId,
            for: indexPath) as? MarkerAnnotationCell else {
            fatalError()
        }
        cell.markerImage.image = self.multiplePinArray[indexPath.row].image
        cell.markerImage.tag = self.multiplePinArray[indexPath.row].index
        if self.imageData.selectedPin == cell.markerImage.tag {
            cell.markerImage.alpha = 1.0
            self.indexPath = indexPath
        } else {
            cell.markerImage.alpha = 0.5
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? MarkerAnnotationCell {
            cell.markerImage.alpha = 0.5
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? MarkerAnnotationCell {
            self.imageData.selectedPin = cell.markerImage.tag
            self.indexPath = indexPath
            NotificationCenter.default.post(name: Notification.Name.NotificationType.selectPinOrCellEvent,
                                        object: cell.markerImage.tag,
                                        userInfo: nil)
            cell.markerImage.alpha = 1.0
        }
    }
}

extension ImageMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
                 calloutAccessoryControlTapped control: UIControl) {
        present(ImageModalViewController(presenter: ImageModalPresenter(
            imageObject: UtilityMethods().getAppDelegate().imageDataModel.selectedAsset!)),
                animated: true,
                completion: nil)
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let customMarker = view as? ImageMarkerView else {
            return
        }
        if self.imageData.selectedPin != customMarker.index {
            self.imageData.selectedPin = customMarker.index
        }
        let coordinate: CLLocation = CLLocation(
            latitude: self.sortedAnnotations[customMarker.index].coordinate.latitude,
            longitude: self.sortedAnnotations[customMarker.index].coordinate.longitude)
        self.populateCollectionView(location: coordinate)
    }

    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let centralLocation = CLLocation(latitude: mapView.centerCoordinate.latitude,
                                         longitude: mapView.centerCoordinate.longitude)
        self.centralLocationCoordinate = centralLocation
    }

    func getRadius() -> Double {
        let topCentralLat: Double = self.centralLocationCoordinate.coordinate.latitude - mapView.region.span.latitudeDelta/2
        let topCentralLocation = CLLocation(latitude: topCentralLat,
                                            longitude: self.centralLocationCoordinate.coordinate.longitude)
        let radius = self.centralLocationCoordinate.distance(from: topCentralLocation)
        return round(radius) / 10.0
    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKCircleRenderer(overlay: overlay)
        renderer.fillColor = UIColor.init(red: 142/255, green: 250/255, blue: 0/255, alpha: 0.2)
        renderer.strokeColor = UIColor.init(red: 142/255, green: 250/255, blue: 0/255, alpha: 0.4)
        renderer.lineWidth = 1.0
        return renderer
    }
}

class MarkerAnnotationCell: UICollectionViewCell {
    let markerImage: UIImageView = {
        let imageView = UIImageView()
        imageView.frame.size.height = 100.0
        imageView.frame.size.width = 100.0
        imageView.layer.cornerRadius = 8
        imageView.layer.masksToBounds = true
        return imageView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        addViews()
    }

    func addViews() {
        backgroundColor = UIColor.clear
        addSubview(markerImage)
        markerImage.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        markerImage.topAnchor.constraint(equalTo: topAnchor, constant: 5).isActive = true
        markerImage.heightAnchor.constraint(equalToConstant: 90).isActive = true
        markerImage.widthAnchor.constraint(equalToConstant: 90).isActive = true
        markerImage.translatesAutoresizingMaskIntoConstraints = false
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //swiftlint:disable next: file_length
}
