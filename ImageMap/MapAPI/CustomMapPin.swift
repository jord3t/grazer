import MapKit
import Contacts

class CustomMapPin: NSObject, MKAnnotation {
    let title: String?
    let index: Int
    let localIdentifier: String
    let locationName: String
    let coordinate: CLLocationCoordinate2D
    let image: UIImage
    var markerTintColor: UIColor {
        return .cyan
    }

    init(title: String,
         locationName: String,
         coordinate: CLLocationCoordinate2D,
         image: UIImage,
         localIdentifier: String,
         index: Int) {
        self.title = title
        self.localIdentifier = localIdentifier
        self.index = index
        self.locationName = locationName
        self.coordinate = coordinate
        self.image = image
        super.init()
    }

    var subtitle: String? {
        return locationName
    }

    func mapItem() -> MKMapItem {
        let addressDict = [CNPostalAddressStreetKey: subtitle!]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        return mapItem
    }
}
