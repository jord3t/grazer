//
//  DateExtensions.swift
//  ImageMap
//
//  Created by trevor jordet on 8/12/18.
//  Copyright © 2018 jord3t. All rights reserved.
//

import Foundation

extension Date {
    func toString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy hh:mm:ss"
        let dateString = dateFormatter.string(from: self)
        return dateString
    }
}
