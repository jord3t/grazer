//
//  ImageDetail+CoreDataProperties.swift
//  ImageMap
//
//  Created by trevor jordet on 8/13/18.
//  Copyright © 2018 jord3t. All rights reserved.
//
//

import Foundation
import CoreData

extension ImageDetail {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ImageDetail> {
        return NSFetchRequest<ImageDetail>(entityName: "ImageDetail")
    }

    @NSManaged public var titleString: String?
    @NSManaged public var descriptionString: String?
    @NSManaged public var detailedDescription: String?
    @NSManaged public var localIdentifier: String?
    @NSManaged public var addressLookupSuccessful: Bool
    @NSManaged public var latCoord: Double
    @NSManaged public var lonCoord: Double

    @nonobjc public class func getRecordByID(identifier: String, context: NSManagedObjectContext) -> ImageDetail {
        var imageDetailRecord: ImageDetail!
        let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ImageDetail")
        request.predicate = NSPredicate(format: "localIdentifier = %@", identifier)
        request.returnsObjectsAsFaults = false
        do {
            let result: [Any] = try context.fetch(request)
            if result.count > 0 {
                guard let existingRecord: ImageDetail = result[0] as? ImageDetail else {
                    fatalError("Could not cast result as ImageDetail")
                }
                imageDetailRecord = existingRecord
            } else {
                guard let newRecord: ImageDetail = NSEntityDescription.insertNewObject(
                    forEntityName: "ImageDetail", into: context) as? ImageDetail else {
                    fatalError("Could not cast newRecord as ImageDetail")
                }
                newRecord.localIdentifier = identifier
                newRecord.addressLookupSuccessful = false
                imageDetailRecord = newRecord
            }
        } catch {}
        return imageDetailRecord
    }

    @nonobjc public class func convertBackupToJSON(json: JSON, context: NSManagedObjectContext) {
        for record in json.arrayValue {
            let upDateRecord: ImageDetail = self.getRecordByID(
                identifier: record["localIdentifier"].string ?? "",
                context: context)
            upDateRecord.addressLookupSuccessful = record["addressLookupSuccessful"].boolValue
            upDateRecord.descriptionString = record["descriptionString"].string
            upDateRecord.titleString = record["titleString"].string
            upDateRecord.latCoord = record["latString"].double ?? 0.0
            upDateRecord.lonCoord = record["lonString"].double ?? 0.0
            upDateRecord.detailedDescription = record["detailedDescription"].string
        }
        UtilityMethods().saveCoreData(delegate: UtilityMethods().getAppDelegate())
    }

    @nonobjc public class func convertRecordsToJSON(context: NSManagedObjectContext) -> URL {
        var jsonData: [JSON] = []
        let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ImageDetail")
        request.returnsObjectsAsFaults = false
        do {
            let result: [Any] = try context.fetch(request)
            if result.count > 0 {
                for i in 0..<result.count {
                    guard let imageDetailRecord: ImageDetail = result[i] as? ImageDetail else {
                        fatalError("Could not cast result as ImageDetail")
                    }
                    let imageJsonData: JSON = [
                        "titleString": imageDetailRecord.titleString ?? "Image \(i)",
                        "latString": imageDetailRecord.latCoord,
                        "lonString": imageDetailRecord.lonCoord,
                        "descriptionString": imageDetailRecord.descriptionString ?? "",
                        "localIdentifier": imageDetailRecord.localIdentifier ?? "",
                        "descriptionString": imageDetailRecord.descriptionString ?? "",
                        "detailedDescription": imageDetailRecord.detailedDescription ?? "",
                        "addressLookupSuccessful": "\(imageDetailRecord.addressLookupSuccessful)"
                    ]
                    jsonData.append(imageJsonData)
                }
            }
        } catch {}
        let json = JSON(jsonData) // convert network data to json
        let fileUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            .appendingPathComponent("GeoPic Backup Data.txt") // Your json file name
        if let data = try? json.rawData() {
            try? data.write(to: fileUrl)
        }
        return fileUrl
    }
}
