//
//  ImageData.swift
//  ImageMap
//
//  Created by trevor jordet on 8/21/18.
//  Copyright © 2018 jord3t. All rights reserved.
//

import Photos
import Contacts

class ImageData {
    private var phAssetArray: [PHAsset] = [PHAsset]()

    var assetArray: [PHAsset] {
        get {
            return self.phAssetArray
        }
        set {
            self.phAssetArray = newValue
        }
    }

    var selectedValue: Int = -1
    var selectedPin: Int {
        get {
            return selectedValue
        }
        set {
            selectedValue = newValue
            if selectedValue > -1 && self.selectedValue < self.phAssetArray.count {
                self.selectedAsset = self.phAssetArray[self.selectedValue]
            }
        }
    }
    var phAsset: PHAsset?
    var selectedAsset: PHAsset? {
        get {
            return self.phAsset
        }
        set {
            self.phAsset = newValue
            NotificationCenter.default.post(
                name: Notification.Name.NotificationType.selectedAssetChanged,
                object: self.selectedAsset,
                userInfo: nil)
        }
    }

    func getAlbumTitles() -> [String] {
        var albumTitleArray: [String] = ["All"]
        let albumList = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .albumRegular, options: nil)
        for i in 0..<albumList.count {
            let album = albumList.object(at: i)
            albumTitleArray.append(album.localizedTitle!)
        }
        return albumTitleArray
    }

    func setAssetArray() {
        DispatchQueue.global(qos: .background).async {
            self.phAssetArray.removeAll()
            let sortDescriptor: NSSortDescriptor = NSSortDescriptor(key: "creationDate", ascending: true)
            let options = PHFetchOptions()
            options.sortDescriptors = [sortDescriptor]
            let assets: PHFetchResult<PHAsset> = PHAsset.fetchAssets(with: options)
            assets.enumerateObjects({ (object, _, _) in
                if let _: CLLocationCoordinate2D = object.location?.coordinate {
                    self.phAssetArray.append(object)
                }
            })
            self.dispatchNotification()
        }
    }

    func fetchCustomAlbumPhotos(albumName: String) {
        DispatchQueue.global(qos: .background).async {
            self.phAssetArray.removeAll()
            var collection = PHAssetCollection()
            let fetchOptions = PHFetchOptions()
            fetchOptions.predicate = NSPredicate(format: "title = %@", albumName)
            let results: PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album,
                                                                                    subtype: .any,
                                                                                    options: fetchOptions)
            if let firstObject = results.firstObject {
                collection = firstObject
            }
            guard let photos = PHAsset.fetchAssets(in: collection, options: nil) as? PHFetchResult<AnyObject> else {
                fatalError()
            }
            photos.enumerateObjects {(object: AnyObject!, _: Int, _: UnsafeMutablePointer<ObjCBool>) in
                if object is PHAsset {
                    guard let asset = object as? PHAsset else {
                        fatalError()
                    }
                    if let _: CLLocationCoordinate2D = object.location?.coordinate {
                        self.phAssetArray.append(asset)
                    }
                }
                self.dispatchNotification()
            }
        }
    }

    func dispatchNotification() {
        DispatchQueue.main.async {
            NotificationCenter.default.post(
                name: Notification.Name.NotificationType.processImages,
                object: self.phAssetArray,
                userInfo: nil)
            //
            NotificationCenter.default.post(
                name: Notification.Name.NotificationType.openEditViewIfNewImage,
                object: self.phAssetArray,
                userInfo: nil)
        }
    }
}
