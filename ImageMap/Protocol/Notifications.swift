//
//  Notifications.swift
//  ImageMap
//
//  Created by trevor jordet on 8/20/18.
//  Copyright © 2018 jord3t. All rights reserved.
//

extension Notification.Name {
    enum NotificationType {
        static let loadNextImage = Notification.Name(rawValue: "Next Button Tapped")
        static let loadPreviousImage = Notification.Name(rawValue: "Previous Button Tapped")
        static let deactivateNavButtons = Notification.Name(rawValue: "Deactivate Navigation Buttons")
        static let activateNavButtons = Notification.Name(rawValue: "Activate Navigation Buttons")
        static let shareButtonTapped = Notification.Name(rawValue: "Share Button Tapped")
        static let cameraButtonTapped = Notification.Name(rawValue: "Camera Button Tapped")
        static let refreshImageData = Notification.Name(rawValue: "Refresh Image Data")
        static let navigationButtonTapped = Notification.Name(rawValue: "Navigation Button Tapped")
        static let processImages = Notification.Name(rawValue: "Process Images")
        static let selectedAssetChanged = Notification.Name(rawValue: "Selected Asset Changed")
        static let openEditViewIfNewImage = Notification.Name(rawValue: "Open Edit View if New Image Loaded")
        static let inAppPurchaseStateSet = Notification.Name(rawValue: "In App Purchase State Set")
        static let inAppPurchaseCompleted = Notification.Name(rawValue: "In App Purchase Completed")
        static let locationUpdated = Notification.Name(rawValue: "Location Updated")
        static let selectPinOrCellEvent = Notification.Name(rawValue: "Pin or CollectionCell Selected")
    }
}
