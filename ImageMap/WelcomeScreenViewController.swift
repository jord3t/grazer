//
//  WelcomeScreenViewController.swift
//  ImageMap
//
//  Created by trevor jordet on 8/10/18.
//  Copyright © 2018 jord3t. All rights reserved.
//

import UIKit
import MapKit
import Photos
import GoogleMobileAds
import StoreKit
import CoreData
import MobileCoreServices

class WelcomeScreenViewController: UIViewController, CLLocationManagerDelegate {
    let locationManager: CLLocationManager = CLLocationManager()
    @IBOutlet weak var mapButtonView: UIView!
    @IBOutlet weak var removeAdsButton: UIButton!
    @IBOutlet weak var adMobView: UIView!
    var bannerView: GADBannerView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.removeAdsButton.isHidden = true
        self.adMobView.isHidden = true
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.checkForInAppPurchase),
                                               name: NSNotification.Name.NotificationType.inAppPurchaseStateSet,
                                               object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways:
            _ = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: nil)
        case .notDetermined:
            self.locationManager.requestAlwaysAuthorization()
        case .authorizedWhenInUse, .restricted, .denied:
            let alertController = UIAlertController(
                title: "Background Location Access Disabled",
                message: "For the best user experience, please open settings and set location access to Always",
                preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            let openAction = UIAlertAction(title: "Open Settings", style: .default) { (_) in
                if let url: URL = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(url)
                }
            }
            alertController.addAction(openAction)
            self.present(alertController, animated: true, completion: nil)
        }
        self.mapButtonView.layer.cornerRadius = 8.0
        if UtilityMethods().getAppDelegate().inAppPurchaseModel.purchaseState == nil {
            UtilityMethods().getAppDelegate().inAppPurchaseModel.setPurchasedState()
        } else {
            self.checkForInAppPurchase(notification: nil)
        }
    }

    @objc func checkForInAppPurchase(notification: Notification?) {
        let purchaseModel: InAppPurchaseModel = UtilityMethods().getAppDelegate().inAppPurchaseModel
        switch purchaseModel.purchaseState {
        case .purchased?:
            self.removeAdsButton.isHidden = true
            self.adMobView.isHidden = true
            self.bannerView = nil
        default:
            self.bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
            UtilityMethods().addBannerViewToView(self.bannerView, containerView: self.adMobView)
            //test ad: ca-app-pub-3940256099942544/2934735716
            //Live ad: ca-app-pub-2166749111373745/4315546896
            bannerView.adUnitID = "ca-app-pub-2166749111373745/4315546896"
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
            self.removeAdsButton.isHidden = false
            self.adMobView.isHidden = false
        }
    }

    func locationManagerApproved() {
        _ = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: nil)
    }

    @IBAction func settingsButtonTapped(_ sender: UIButton) {
        let settingsViewController = SettingsViewController()
        self.present(settingsViewController, animated: true, completion: nil)
    }

    @IBAction func goToMap(_ sender: UIButton) {
        let imageMapViewController = ImageMapViewController()
        self.navigationController?.pushViewController(imageMapViewController, animated: true)
    }
    @IBAction func removeAdsButtonTapped(_ sender: UIButton) {
        let removeAdsViewController = InAppPurchaseViewController()
        self.present(removeAdsViewController, animated: true, completion: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self,
                                               name: NSNotification.Name.NotificationType.inAppPurchaseStateSet,
                                               object: nil)
    }
}
