//
//  GeoPicProducts.swift
//  ImageMap
//
//  Created by trevor jordet on 8/25/18.
//  Copyright © 2018 jord3t. All rights reserved.
//

import Foundation

public struct GeoPicProducts {
    public static let removeAds = "com.jord3t.geopic.removeads"
    private static let productIdentifiers: Set<ProductIdentifier> = [GeoPicProducts.removeAds]
    public static let store = IAPHelper(productIds: GeoPicProducts.productIdentifiers)
}

func resourceNameForProductIdentifier(_ productIdentifier: String) -> String? {
    return productIdentifier.components(separatedBy: ".").last
}
