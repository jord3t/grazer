//
//  BottomNavViewController.swift
//  ImageMap
//
//  Created by trevor jordet on 8/20/18.
//  Copyright © 2018 jord3t. All rights reserved.
//

import UIKit
import GoogleMobileAds

enum Direction: String {
    case nextImage
    case previousImage
}

class BottomNavViewController: UIViewController {
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var adMobView: UIView!
    var bannerView: GADBannerView!
    let purchaseModel: InAppPurchaseModel = UtilityMethods().getAppDelegate().inAppPurchaseModel
    var removeAdForThisView: Bool = false
    var navDirection: Direction = .nextImage

    override func viewDidLoad() {
        super.viewDidLoad()
        if self.removeAdForThisView {
            self.adMobView.isHidden = true
        } else {
            NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.checkForInAppPurchase),
                                               name: NSNotification.Name.NotificationType.inAppPurchaseStateSet,
                                               object: nil)
            if self.purchaseModel.purchaseState == nil {
                self.purchaseModel.setPurchasedState()
            } else {
                self.checkForInAppPurchase(notification: nil)
            }
        }

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.disableNavButtons),
                                               name: NSNotification.Name.NotificationType.deactivateNavButtons,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.enableNavButtons),
                                               name: NSNotification.Name.NotificationType.activateNavButtons,
                                               object: nil)
    }

    @objc func checkForInAppPurchase(notification: Notification?) {
        switch self.purchaseModel.purchaseState {
        case .purchased?:
            self.adMobView.isHidden = true
        case .notPurchased?, .notAvailable?, .none:
            self.bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
            UtilityMethods().addBannerViewToView(self.bannerView, containerView: self.adMobView)
            //test ad: ca-app-pub-3940256099942544/2934735716
            //Live ad: ca-app-pub-2166749111373745/4315546896
            bannerView.adUnitID = "ca-app-pub-2166749111373745/4315546896"
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
        }
    }

    @IBAction func previousButtonTapped(_ sender: UIButton) {
        self.navDirection = .previousImage
        NotificationCenter.default.post(
            name: Notification.Name.NotificationType.loadPreviousImage,
            object: nil,
            userInfo: nil)
    }

    @IBAction func nextButtonTapped(_ sender: UIButton) {
        self.navDirection = .nextImage
        NotificationCenter.default.post(
            name: Notification.Name.NotificationType.loadNextImage,
            object: nil,
            userInfo: nil)
    }

    @IBAction func navigationButtonTapped(_ sender: UIButton) {
        NotificationCenter.default.post(
            name: Notification.Name.NotificationType.navigationButtonTapped,
            object: nil,
            userInfo: nil)
    }

    @IBAction func refreshButtonTapped(_ sender: UIButton) {
        NotificationCenter.default.post(
            name: Notification.Name.NotificationType.refreshImageData,
            object: nil,
            userInfo: nil)
    }

    @IBAction func cameraButtonTapped(_ sender: UIButton) {
        NotificationCenter.default.post(
            name: Notification.Name.NotificationType.cameraButtonTapped,
            object: nil,
            userInfo: nil)
    }

    @IBAction func shareButtonTapped(_ sender: UIButton) {
        NotificationCenter.default.post(
            name: Notification.Name.NotificationType.shareButtonTapped,
            object: nil,
            userInfo: nil)
    }

    @objc func disableNavButtons(notificatin: Notification) {
        self.nextButton.isEnabled = false
        self.previousButton.isEnabled = false
    }

    @objc func enableNavButtons(notificatin: Notification) {
        self.nextButton.isEnabled = true
        self.previousButton.isEnabled = true
    }

    deinit {
        NotificationCenter.default.removeObserver(self,
                                               name: NSNotification.Name.NotificationType.deactivateNavButtons,
                                               object: nil)
        NotificationCenter.default.removeObserver(self,
                                               name: NSNotification.Name.NotificationType.activateNavButtons,
                                               object: nil)
        NotificationCenter.default.removeObserver(self,
                                               name: NSNotification.Name.NotificationType.inAppPurchaseStateSet,
                                               object: nil)
    }
}
