//
//  PHAssetExtensions.swift
//  ImageMap
//
//  Created by trevor jordet on 8/10/18.
//  Copyright © 2018 jord3t. All rights reserved.
//

import Photos

extension PHAsset {
    func getUIResizedImage(width: Int, height: Int, contentMode: PHImageContentMode) -> UIImage {
        var thumbnail = UIImage()
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        option.isSynchronous = true
        manager.requestImage(for: self,
                             targetSize: CGSize(width: width, height: height),
                             contentMode: contentMode,
                             options: option,
                             resultHandler: {(result, _) -> Void in
            thumbnail = result!
        })
        return thumbnail
    }

    func getUIFullScaleImage() -> UIImage {
        var scaledImage = UIImage()
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        option.isSynchronous = true
        let pxHgt: Double = Double(self.pixelHeight)
        let pxWdt: Double = Double(self.pixelWidth)
        let imgSize: CGSize = CGSize(width: pxHgt, height: pxWdt)
        manager.requestImage(for: self,
                             targetSize: imgSize,
                             contentMode: .aspectFit,
                             options: option,
                             resultHandler: {(result, _) -> Void in
            scaledImage = result!
        })
        return scaledImage
    }
}
