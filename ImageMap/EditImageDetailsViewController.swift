//
//  EditImageDetailsViewController.swift
//  ImageMap
//
//  Created by trevor jordet on 8/15/18.
//  Copyright © 2018 jord3t. All rights reserved.
//

import UIKit
import Photos

protocol EditImageDetailsView {
    func getView() -> UIView
}

class EditImageDetailsViewController: BaseViewController, EditImageDetailsView {
    func getView() -> UIView {
        return self.view
    }

    // MARK: Properties
    var presenter: EditImageDetailsPresenter

    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var bottomNavView: UIView!

    // MARK: Initializing
    init(presenter: EditImageDetailsPresenter) {
        self.presenter = presenter
        super.init()
        self.presenter.view = self.view
        self.imageView.contentMode = .scaleAspectFill
        self.imageView.center = self.view.center
        self.imageView.image = presenter.getImage()
        self.titleText.text = presenter.imageRecord.titleString ?? ""
        self.descriptionText.text = presenter.imageRecord.detailedDescription ?? ""
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.descriptionText.layer.cornerRadius = 5.0
        let navVC = BottomNavViewController()
        self.addChild(navVC)
        navVC.view.frame = self.bottomNavView.bounds
        self.bottomNavView.addSubview(navVC.view)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.imageChanged),
                                               name: NSNotification.Name.NotificationType.selectedAssetChanged,
                                               object: nil)
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipe))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipe))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(swipeLeft)
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipe))
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        self.view.addGestureRecognizer(swipeDown)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UtilityMethods().getAppDelegate().inAppPurchaseModel.purchaseState == .purchased {
            UtilityMethods().resizeContainer(containerView: self.bottomNavView, newHeight: 50.0)
        }
    }

    @objc override func dismissViewControllers() {
        self.dismiss(animated: false, completion: {NotificationCenter.default.post(
            name: Notification.Name.NotificationType.cameraButtonTapped,
            object: nil,
            userInfo: nil)})
    }

    @objc func cameraLaunched(notification: Notification   ) {
        self.dismiss(animated: false, completion: nil)
    }

    @objc func respondToSwipe(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                NotificationCenter.default.post(
                    name: Notification.Name.NotificationType.loadPreviousImage,
                    object: nil,
                    userInfo: nil)
            case UISwipeGestureRecognizer.Direction.left:
                NotificationCenter.default.post(
                    name: Notification.Name.NotificationType.loadNextImage,
                    object: nil,
                    userInfo: nil)
            case UISwipeGestureRecognizer.Direction.down:
                self.dismiss(animated: true, completion: nil)
            default: break
            }
        }
    }

    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func saveButtonTapped(_ sender: UIButton) {
        self.presenter.imageRecord.titleString = self.titleText.text
        self.presenter.imageRecord.detailedDescription = self.descriptionText.text
        UtilityMethods().saveCoreData(delegate: UtilityMethods().getAppDelegate())
        NotificationCenter.default.post(
            name: Notification.Name.NotificationType.refreshImageData,
            object: nil,
            userInfo: nil)
        dismiss(animated: true, completion: nil)
    }

    @objc func imageChanged(notification: Notification   ) {
        guard let imageAsset: PHAsset = notification.object as? PHAsset else {
            fatalError("Could not cast notification.object as PHAsset")
        }
        self.presenter.setImage(imageObject: imageAsset)
        self.imageView.image = presenter.getImage()
        self.titleText.text = presenter.imageRecord.titleString ?? ""
        self.descriptionText.text = presenter.imageRecord.detailedDescription ?? ""
    }

    @IBAction func deleteCurrentImage(_ sender: UIButton) {
        let assetsToDelete: NSFastEnumeration = [self.presenter.imageAsset] as NSFastEnumeration
        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.deleteAssets(assetsToDelete)
        }, completionHandler: { (success: Bool, _: Error?) in
            if success == true {
                UtilityMethods().getAppDelegate().persistentContainer.viewContext.delete(self.presenter.imageRecord)
                DispatchQueue.main.async {
                    UtilityMethods().getAppDelegate().imageDataModel.selectedPin -= 1
                    self.dismiss(animated: true, completion: {NotificationCenter.default.post(
                        name: Notification.Name.NotificationType.refreshImageData,
                        object: nil,
                        userInfo: nil)})
                }
            }
        })
    }

    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.NotificationType.selectedAssetChanged,
                                                  object: nil)
    }
}

class EditImageDetailsPresenter {
    // MARK: Properties
    var view: UIView!
    var imageAsset: PHAsset!
    var imageRecord: ImageDetail!
    init(imageObject: PHAsset) {
        self.imageAsset = imageObject
        self.imageRecord = ImageDetail.getRecordByID(
            identifier: self.imageAsset.localIdentifier,
            context: UtilityMethods().getAppDelegate().persistentContainer.viewContext)
    }

    func setImage(imageObject: PHAsset) {
        self.imageAsset = imageObject
        self.imageRecord = ImageDetail.getRecordByID(
            identifier: self.imageAsset.localIdentifier,
            context: UtilityMethods().getAppDelegate().persistentContainer.viewContext)
    }

    func getImage() -> UIImage {
        return self.imageAsset.getUIResizedImage(width: Int(self.view.frame.width),
                                          height: Int(self.view.frame.height),
                                          contentMode: .aspectFit)
    }
}
