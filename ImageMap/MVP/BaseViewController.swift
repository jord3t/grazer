//
//  BasePresenter.swift
//  ImageMap
//
//  Created by trevor jordet on 8/12/18.
//  Copyright © 2018 jord3t. All rights reserved.
//

import UIKit
import CoreLocation
import Photos
import MessageUI
import Contacts
import CoreData
import MobileCoreServices

class BaseViewController: UIViewController {
    var installedNavigationApps: [String: String] = [:]
    var navigationAppUrlList: [String: String] = [:]
    var installedSmsApps: [String: String] = [:]
    var smsAppUrlList: [String: String] = [:]
    var locationManager = CLLocationManager()
    var deviceLocation: CLLocation!
    var documentInteractionController: UIDocumentInteractionController!

    // MARK: Initializing

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if CLLocationManager .locationServicesEnabled() {
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            self.locationManager.requestWhenInUseAuthorization()
            self.locationManager.startUpdatingLocation()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.launchGPSapp),
                                               name: NSNotification.Name.NotificationType.navigationButtonTapped,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.launchSMSapp),
                                               name: NSNotification.Name.NotificationType.shareButtonTapped,
                                               object: nil)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.openCameraViewController),
                                               name: NSNotification.Name.NotificationType.cameraButtonTapped,
                                               object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self,
                                               name: NSNotification.Name.NotificationType.navigationButtonTapped,
                                               object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.NotificationType.shareButtonTapped,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.NotificationType.cameraButtonTapped,
                                                  object: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc func launchGPSapp(notification: Notification) {
        let location: CLLocation = (UtilityMethods().getAppDelegate().imageDataModel.selectedAsset?.location)!
        var latCoord = location.coordinate.latitude
        var lonCoord = location.coordinate.longitude
        let imageRecord: ImageDetail! = ImageDetail.getRecordByID(
            identifier: (UtilityMethods().getAppDelegate().imageDataModel.selectedAsset?.localIdentifier)!,
            context: UtilityMethods().getAppDelegate().persistentContainer.viewContext)
        if imageRecord.latCoord != 0.0 && imageRecord.lonCoord != 0.0 {
            latCoord = imageRecord.latCoord
            lonCoord = imageRecord.lonCoord
        }
        let location2D: CLLocationCoordinate2D = CLLocationCoordinate2D(
            latitude: latCoord,
            longitude: lonCoord)
        self.installedNavigationApps.removeAll()
        self.navigationAppUrlList = [
        "Apple Maps": "http://maps.apple.com/?daddr=\(location2D.latitude),\(location2D.longitude)",
        "Google Maps": "comgooglemaps://?daddr=\(location2D.latitude),\(location2D.longitude)",
        "Waze": "waze://?ll=\(location2D.latitude),\(location2D.longitude)&navigate=yes"]

        for app in navigationAppUrlList {
            if appIsInstalled(urlString: app.value) {
                self.installedNavigationApps[app.key] = app.value
            }
        }
        let alert = UIAlertController(title: "Navigate to This Location",
                                      message: "Select a GPS application:",
                                      preferredStyle: .actionSheet)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.width - 80,
                                                                 y: self.view.bounds.height - 50,
                                                                 width: 0, height: 0)

        for appItem in installedNavigationApps {
            let button = UIAlertAction(title: appItem.key, style: .default, handler: self.launchNavigationApp)
            alert.addAction(button)
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel)
        alert.addAction(cancelButton)
        self.present(alert, animated: true, completion: nil)
    }

    func launchNavigationApp(alert: UIAlertAction) {
        let urlString: String = self.installedNavigationApps[alert.title!]!
        UIApplication.shared.open(URL(string: urlString)!,
                                  options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]),
                                  completionHandler: nil)
    }

    func appIsInstalled(urlString: String) -> Bool {
        let url: URL = URL(string: urlString)!
        if UIApplication.shared.canOpenURL(url) {
            return true
        }
        return false
    }
}

extension BaseViewController {
    @objc func launchSMSapp(notification: Notification) {
        self.installedSmsApps.removeAll()
        self.smsAppUrlList = ["WhatsApp": "whatsapp://app"]
        for app in smsAppUrlList {
            if appIsInstalled(urlString: app.value) {
                self.installedSmsApps[app.key] = app.value
            }
        }
        self.installedSmsApps["Share"] = "Share always works"
        let alert = UIAlertController(title: "Send This Image",
                                      message: "Select a messaging application:",
                                      preferredStyle: .actionSheet)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: 80,
                                                                 y: self.view.bounds.height - 50,
                                                                 width: 0,
                                                                 height: 0)
        for appItem in installedSmsApps {
            let button = UIAlertAction(title: appItem.key, style: .default, handler: self.sendSMSText)
            alert.addAction(button)
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel)
        alert.addAction(cancelButton)
        self.present(alert, animated: true, completion: nil)
    }
}

extension BaseViewController: MFMessageComposeViewControllerDelegate, UIDocumentInteractionControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController,
                                      didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }

    @objc func sendSMSText(alert: UIAlertAction) {
        let selectedAsset: PHAsset = UtilityMethods().getAppDelegate().imageDataModel.selectedAsset!
        let selectedRecord: ImageDetail = ImageDetail.getRecordByID(
            identifier: selectedAsset.localIdentifier,
            context: UtilityMethods().getAppDelegate().persistentContainer.viewContext)
        let image: UIImage = selectedAsset.getUIFullScaleImage()
        let smsAppTitle: String = alert.title!
        var messageBody: String = selectedRecord.titleString ?? ""
        messageBody += "\n\(selectedRecord.descriptionString ?? "")"
        if selectedRecord.detailedDescription != nil {
            messageBody += "\n\(selectedRecord.detailedDescription!)"
        }
        messageBody += "\n\nSent from GeoPic."
        switch smsAppTitle {
        case "WhatsApp":
            let pathURL =   URL.init(fileURLWithPath: NSHomeDirectory() + "/Documents/whatsAppTmp.jpg")
            do {
                try image.jpegData(compressionQuality: 1.0)?.write(to: pathURL)
            } catch {
                print(error.localizedDescription)
            }
            self.documentInteractionController = UIDocumentInteractionController.init()

            self.documentInteractionController.url = pathURL
            self.documentInteractionController.uti = "net.whatsapp.image"
            self.documentInteractionController.delegate = self
            self.documentInteractionController.presentOpenInMenu(
                from: CGRect(x: 0, y: 0, width: 0, height: 0),
                in: self.view, animated: true)
        default:
            self.share(shareText: messageBody, shareImage: image)
        }
    }

    func share(shareText: String?, shareImage: UIImage?) {

        var objectsToShare = [AnyObject]()

        if let shareTextObj = shareText {
            objectsToShare.append(shareTextObj as AnyObject)
        }

        if let shareImageObj = shareImage {
            objectsToShare.append(shareImageObj)
        }

        if shareText != nil || shareImage != nil {
            let activityViewController = UIActivityViewController(
                activityItems: objectsToShare,
                applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            present(activityViewController, animated: true, completion: nil)
        }
    }
}

extension BaseViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @objc func openCameraViewController(notification: Notification) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            self.dismissViewControllers()
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.mediaTypes = [kUTTypeMovie as String, kUTTypeImage as String]
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }

    @objc func dismissViewControllers() {
        //override this method to close edit view controller
    }
    //swiftlint:disable next: function_body_length
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        guard info[UIImagePickerController.InfoKey.mediaType] != nil else { return }
        //swiftlint:disable next: force_cast
        let mediaType: CFString = info[UIImagePickerController.InfoKey.mediaType] as! CFString
        // Local variable inserted by Swift 4.2 migrator.
        let info2 = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        //swiftlint:disable next: line_length
        let imageMetadata = info2[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaMetadata)] as? [AnyHashable: Any]
        let gpsMetaData = self.createGPSdictionary(metadataAsMutable: imageMetadata, location: self.deviceLocation)
        switch mediaType {
        case kUTTypeImage:
            //swiftlint:disable next: line_length
            guard let newImage: UIImage = info2[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage else {
                fatalError("Could not cast info[UIImagePickerControllerOriginalImage] as UIImage")
            }
            PHPhotoLibrary.shared().performChanges({() -> Void in
                let source: CGImageSource = CGImageSourceCreateWithData(
                    newImage.jpegData(compressionQuality: 1)! as NSData, nil)!
                let UTI: CFString = CGImageSourceGetType(source)!
                let newImageData = NSMutableData()
                let destination: CGImageDestination = CGImageDestinationCreateWithData(
                    (newImageData as CFMutableData),
                    UTI, 1,
                    nil)!
                CGImageDestinationAddImageFromSource(destination, source, 0, gpsMetaData! as CFDictionary)
                CGImageDestinationFinalize(destination)
                PHPhotoLibrary.shared().performChanges({
                    let creationRequest = PHAssetCreationRequest.forAsset()
                    creationRequest.addResource(with: .photo, data: newImageData as Data, options: nil)
                }, completionHandler: { (success: Bool, _: Error?) in
                    if success == true {
                        DispatchQueue.main.async {
                            self.refreshImageDataAndOpenEditView()
                        }
                    }
                })
                })
        case kUTTypeMovie:
            guard
                let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String,
                mediaType == (kUTTypeMovie as String),
                let url = info[UIImagePickerController.InfoKey.mediaURL] as? URL,
                UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(url.path)
                else {
                    return
            }
            // Handle a movie capture
            UISaveVideoAtPathToSavedPhotosAlbum(
                url.path,
                self,
                #selector(video(_:didFinishSavingWithError:contextInfo:)),
                nil)
        case kUTTypeLivePhoto:
            break
        default:
            break
        }
    }

    func setVideoLocation() {
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        fetchOptions.fetchLimit = 1
        let fetchResult = PHAsset.fetchAssets(with: .video, options: fetchOptions)
        //`fetchResult` is not nil here (without using `if-let`)
        if let asset = fetchResult.firstObject {
            PHPhotoLibrary.shared().performChanges({
                let changeRequest: PHAssetChangeRequest = PHAssetChangeRequest.init(for: asset)
                changeRequest.location = self.deviceLocation
            }, completionHandler: { (success: Bool, _: Error?) in
                if success == true {
                    NotificationCenter.default.post(
                        name: Notification.Name.NotificationType.refreshImageData,
                        object: nil,
                        userInfo: nil)
                    sleep(1)
                    NotificationCenter.default.post(
                        name: Notification.Name.NotificationType.loadNextImage,
                        object: nil,
                        userInfo: nil)
                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            })
        }
    }

    @objc func video(_ videoPath: String, didFinishSavingWithError error: Error?, contextInfo info: AnyObject) {
        self.setVideoLocation()
    }

    func createGPSdictionary(metadataAsMutable: [AnyHashable: Any]?, location: CLLocation) -> [AnyHashable: Any]? {
        var metadata = metadataAsMutable
        var GPSDictionary = (metadata?[(kCGImagePropertyGPSDictionary as String)]) as? [AnyHashable: Any]
        if GPSDictionary == nil {
            GPSDictionary = [AnyHashable: Any]()
        }
        GPSDictionary?[(kCGImagePropertyGPSLatitude as String)] = self.convertToPositive(
            location.coordinate.latitude)
        GPSDictionary?[(kCGImagePropertyGPSLatitudeRef as String)] = self.getLatitudeRefDirection(
            location.coordinate.latitude)
        GPSDictionary?[(kCGImagePropertyGPSLongitude as String)] = self.convertToPositive(
            location.coordinate.longitude)
        GPSDictionary?[(kCGImagePropertyGPSLongitudeRef as String)] = self.getLongtitudeRefDirection(
            location.coordinate.longitude)
        GPSDictionary?[(kCGImagePropertyGPSAltitude as String)] = location.altitude
        GPSDictionary?[(kCGImagePropertyGPSTimeStamp as String)] = Date()
        GPSDictionary?[(kCGImagePropertyGPSDateStamp as String)] = Date()
        metadata?[(kCGImagePropertyGPSDictionary as String)] = GPSDictionary
        metadata?[(kCGImagePropertyGPSDictionary as String)] = GPSDictionary
        return metadata
    }

    func getLatitudeRefDirection(_ lat: Double) -> String {
        if lat > 0 {
            return "N"
        } else {
            return "S"
        }
    }

    func getLatitudeValue(_ latitudeRef: String, _ latitude: Double) -> Double {
        if latitudeRef == "S" {
            if latitude > 0 {
                return (latitude * -1)
            }
            return (latitude)
        } else {
            return latitude
        }
    }

    func getLongtitudeRefDirection(_ long: Double) -> String {
        if long > 0 {
            return "E"
        } else {
            return "W"
        }
    }

    func getLongitudeValue(_ longitudeRef: String, _ longitude: Double) -> Double {
        if longitudeRef == "W"{
            if longitude > 0 {
                return (longitude * -1)
            }
            return (longitude)
        } else {
            return longitude
        }
    }

    func convertToPositive(_ val: Double) -> Double {
         if val > 0 {
            return val
        } else {
            return (val * -1)
        }
    }

    func refreshImageDataAndOpenEditView() {
        NotificationCenter.default.post(
            name: Notification.Name.NotificationType.refreshImageData,
            object: nil,
            userInfo: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.selectLastImageAndEdit),
                                               name: NSNotification.Name.NotificationType.openEditViewIfNewImage,
                                               object: nil)
    }

    @objc func selectLastImageAndEdit(notification: Notification) {
        NotificationCenter.default.removeObserver(self,
                                               name: NSNotification.Name.NotificationType.openEditViewIfNewImage,
                                               object: nil)
        let imageData: ImageData = UtilityMethods().getAppDelegate().imageDataModel
        imageData.selectedPin = imageData.assetArray.count - 1
        self.dismiss(animated: true, completion: {})
        present(EditImageDetailsViewController(
                presenter: EditImageDetailsPresenter(imageObject: imageData.selectedAsset!)),
                animated: true,
                completion: nil)
    }
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default))
        self.present(alertController, animated: true, completion: nil)
    }
}

extension BaseViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation] ) {
        let locationArray = locations as NSArray
        guard let locationObj = locationArray.lastObject as? CLLocation else {
            fatalError("Could not cast locationArray.lastObject as! CLLocation")
        }
        let initialLocation = CLLocation(latitude: locationObj.coordinate.latitude,
                                         longitude: locationObj.coordinate.longitude)
        self.deviceLocation = initialLocation
        NotificationCenter.default.post(
            name: Notification.Name.NotificationType.locationUpdated,
            object: nil,
            userInfo: nil)
        self.locationManager.stopUpdatingLocation()
    }
}

// Helper function inserted by Swift 4.2 migrator.
private func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
private func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
private func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
    //swiftlint:disable next: file_length
}
