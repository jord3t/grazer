//
//  UIImageViewExtensions.swift
//  ImageMap
//
//  Created by trevor jordet on 9/4/18.
//  Copyright © 2018 jord3t. All rights reserved.
//

extension UIImageView {
    func copyView<T: UIImageView>() -> T {
        guard let copy: T = NSKeyedUnarchiver.unarchiveObject(
            with: NSKeyedArchiver.archivedData(withRootObject: self)) as? T else {
            fatalError()
        }
        return copy
    }
}
