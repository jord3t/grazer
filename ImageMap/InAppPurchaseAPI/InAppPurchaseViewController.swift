//
//  InAppPurchaseViewController.swift
//  ImageMap
//
//  Created by trevor jordet on 8/25/18.
//  Copyright © 2018 jord3t. All rights reserved.
//

import UIKit
import StoreKit

class InAppPurchaseViewController: UIViewController {
    var greenColor: UIColor!
    @IBOutlet weak var removeAdsButton: UIButton!
    var purchaseModel: InAppPurchaseModel!
    let priceFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.formatterBehavior = .behavior10_4
        formatter.numberStyle = .currency
        return formatter
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.purchaseModel = UtilityMethods().getAppDelegate().inAppPurchaseModel
        self.greenColor = self.removeAdsButton.backgroundColor!
        self.removeAdsButton.layer.cornerRadius = 8.0
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.checkForInAppPurchase),
                                               name: NSNotification.Name.NotificationType.inAppPurchaseStateSet,
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.setPurchaseState(_:)),
                                               name: Notification.Name.IAPHelperPurchaseNotification,
                                               object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.removeAdsButton.backgroundColor = UIColor.gray
        self.removeAdsButton.isEnabled = false
        self.removeAdsButton.setTitle("loading products", for: .disabled)
        if self.purchaseModel.purchaseState == nil {
           self.purchaseModel.setPurchasedState()
        } else {
            self.checkForInAppPurchase(notification: nil)
        }
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipe))
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        self.view.addGestureRecognizer(swipeDown)
    }
    @objc func setPurchaseState(_ notification: Notification) {
        self.purchaseModel.setPurchasedState()
    }

    @objc func checkForInAppPurchase(notification: Notification?) {
        switch purchaseModel.purchaseState {
        case .purchased?:
            self.removeAdsButton.setTitle("Thank You!", for: .disabled)
            self.removeAdsButton.backgroundColor = UIColor.gray
            self.removeAdsButton.isEnabled = false
        case .notPurchased?:
            if IAPHelper.canMakePayments() {
                self.priceFormatter.locale = purchaseModel.product?.priceLocale
                self.removeAdsButton.isEnabled = true
                self.removeAdsButton.backgroundColor = self.greenColor
                let priceString: String = self.priceFormatter.string(from: (purchaseModel.product?.price)!)!
                let productName: String = (purchaseModel.product?.localizedTitle)!
                self.removeAdsButton.setTitle("\(productName) \(priceString)", for: .normal)
            }
        default:
            self.removeAdsButton.backgroundColor = UIColor.gray
            self.removeAdsButton.isEnabled = false
            self.removeAdsButton.setTitle("Not available", for: .disabled)
        }
    }

    @IBAction func removeAdButtonTapped(_ sender: UIButton) {
        GeoPicProducts.store.buyProduct(UtilityMethods().getAppDelegate().inAppPurchaseModel.products[0])
    }
    @IBAction func ancelButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    @objc func respondToSwipe(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.down:
                self.dismiss(animated: true, completion: nil)
            default: break
            }
        }
    }
}
