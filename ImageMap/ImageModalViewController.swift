//
//  ImageModalViewController.swift
//  ImageMap
//
//  Created by trevor jordet on 8/12/18.
//  Copyright © 2018 jord3t. All rights reserved.
//

import UIKit
import Photos
import AVKit

protocol ImageModalView {
    func getView() -> UIView
    func setImage(image: UIImage)
}
// swiftlint:disable:next type_body_length
class ImageModalViewController: BaseViewController, ImageModalView, UIGestureRecognizerDelegate {
    func getView() -> UIView {
        return self.view
    }

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var tempImageView: UIImageView!
    @IBOutlet weak var titleDescriptionView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var bottomNavView: UIView!
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    @IBOutlet var doubleTapGesture: UITapGestureRecognizer!
    @IBOutlet var panGesture: UIPanGestureRecognizer!

    // MARK: AnimationConstraints
    @IBOutlet weak var tempImageHeight: NSLayoutConstraint!
    @IBOutlet weak var tempImageWidth: NSLayoutConstraint!
    @IBOutlet weak var tempImageCenterY: NSLayoutConstraint!
    @IBOutlet weak var tempImageCenterX: NSLayoutConstraint!

    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    @IBOutlet weak var imageCenterY: NSLayoutConstraint!
    @IBOutlet weak var imageCenterX: NSLayoutConstraint!
    @IBOutlet weak var titleDescriptionHeight: NSLayoutConstraint!

    // MARK: Properties
    var presenter: ImageModalPresenter
    var image: UIImage!
    var tempImage: UIImage!
    var descriptionHeight: CGFloat = 40.0
    var hideControls: Bool = true
    var originalWidth: CGFloat!
    var originalHeight: CGFloat!
    var fillImageMode: Bool = true
    var directionX: CGFloat = 0
    var firstLoad: Bool = false
    var originY: CGFloat = 0
    var swipingImage: Bool = false
    let navVC = BottomNavViewController()
    var swipeThreshold: CGFloat = 150.00

    // MARK: Initializing
    init(presenter: ImageModalPresenter) {
        self.presenter = presenter
        super.init()
        self.presenter.view = self.view
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.imageChanged),
                                               name: NSNotification.Name.NotificationType.selectedAssetChanged,
                                               object: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        navVC.removeAdForThisView = true
        self.addChild(self.navVC)
        navVC.view.frame = self.bottomNavView.bounds
        self.bottomNavView.addSubview(navVC.view)

        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipe))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipe))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(swipeLeft)

        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipe))
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        self.view.addGestureRecognizer(swipeDown)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.openCameraViewController),
                                               name: NSNotification.Name.NotificationType.cameraButtonTapped,
                                               object: nil)
        self.tapGesture.require(toFail: self.doubleTapGesture)
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
        } catch {
            print("Setting category to AVAudioSessionCategoryPlayback failed.")
        }

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let _: Timer = Timer.scheduledTimer(
            timeInterval: 0.1,
            target: self,
            selector: #selector(self.setSetImageAfterAdLoads),
            userInfo: nil,
            repeats: false)
    }
    @objc func setSetImageAfterAdLoads() {
        if self.imageView.image == nil {
            self.firstLoad = true
            self.setImage(image: self.presenter.getImage())
            self.setDescriptionState()
            self.showHidePlayButton()
        }
    }

    func showHidePlayButton() {
        if self.presenter.imageAsset.mediaType == .video {
            self.playButton.isHidden = false
        } else {
            self.playButton.isHidden = true
        }
    }

    @objc func imageChanged(notification: Notification   ) {
        self.matchImageDimensions()
        if self.navVC.navDirection == .previousImage {
            self.directionX = self.imageCenterX.constant + self.imageView.frame.width/2
        } else {
            self.directionX = self.imageCenterX.constant - self.imageView.frame.width/2
        }
        self.tempImageView.isHidden = false
        self.tempImageView.alpha = 1.0
        self.imageView.alpha = 0.0
        guard let imageAsset: PHAsset = notification.object as? PHAsset else {
            fatalError("Could not cast notification.object as PHAsset")
        }
        self.presenter.setImage(imageObject: imageAsset)
        self.imageView.image = nil
        self.setImage(image: self.presenter.getImage())
    }

    @IBAction func playVideo(_ sender: UIButton) {
        self.presenter.imageAsset.requestContentEditingInput(with: PHContentEditingInputRequestOptions(),
                                                             completionHandler: { (contentEditingInput, _) in
            if let strURL = (contentEditingInput!.audiovisualAsset as? AVURLAsset)?.url.absoluteString {
                let player = AVPlayer(url: URL(string: strURL)!)
                let vc = AVPlayerViewController()
                vc.player = player
                self.present(vc, animated: true) {
                    vc.player?.play()
                }
            }
        })
    }
    func setImage(image: UIImage) {
        self.originalHeight = self.bottomNavView.frame.origin.y - 32.0
        self.imageView.contentMode = .scaleAspectFit
        self.image = image
        self.imageView.image = self.image
        let ratio: CGFloat = self.originalHeight/self.image.size.height
        self.originalWidth = self.image.size.width * ratio
        UtilityMethods().resizeImageView(
            containerView: self.imageView,
            newWidth: self.originalWidth,
            newHeight: self.originalHeight)
        if self.fillImageMode {
            self.fillImage()
        } else {
            self.fitImage()
        }
        self.imageView.alpha = 0
        self.imageView.fadeOut(duration: 0.0, delay: 0.1, completion: {(_: Bool) -> Void in
            if !self.firstLoad {
                if self.navVC.navDirection == .nextImage {
                    self.imageCenterX.constant = self.view.frame.size.width/2 + self.imageView.frame.width/2
                } else {
                    self.imageCenterX.constant = 0 - self.imageView.frame.width/2
                }
            }
            self.firstLoad = false
            self.setOrigin()
            self.showHidePlayButton()
        })
    }

    @IBAction func showDescription(_ sender: UISwipeGestureRecognizer) {
        if sender.view != nil {
            self.descriptionHeight = 150.0
            self.setDescriptionState(false)
        }
    }

    @IBAction func hideDescription(_ sender: UISwipeGestureRecognizer) {
        if sender.view != nil {
            self.descriptionTextView.isHidden = true
            self.descriptionHeight = 40.0
            self.setDescriptionState(false)
        }
    }

    @objc func setDescriptionState(_ fade: Bool = true) {
        // swiftlint:disable:next line_length
        self.titleLabel.text = self.presenter.imageRecord.titleString ?? "Image \(UtilityMethods().getAppDelegate().imageDataModel.selectedPin)"
        if fade {
            self.fadeInDescription()
        } else {
            self.setDescriptionSize()
        }
    }

    func fadeInDescription() {
        self.titleDescriptionView.fadeOut(
            duration: 0.0,
            completion: {(_: Bool) -> Void in
                self.setDescriptionSize()
        })
    }

    func setDescriptionSize() {
        self.titleDescriptionView.isHidden = self.hideControls
        self.titleDescriptionHeight.constant = self.descriptionHeight
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.view.layoutIfNeeded()
            }, completion: {(_: Bool) -> Void in
                if self.descriptionHeight < 150.0 {
                    self.descriptionTextView.isHidden = true
                } else {
                    self.descriptionTextView.isHidden = false
                    var descriptionString: String = "\(self.presenter.imageRecord.descriptionString ?? "")"
                    if self.presenter.imageRecord.detailedDescription != nil {
                        descriptionString += "\n\(self.presenter.imageRecord.detailedDescription!)"
                    }
                    self.descriptionTextView.text = descriptionString
                }
                self.titleDescriptionView.frame.origin.x = 0.0
                self.titleDescriptionView.fadeIn(
                    duration: 0.20,
                    delay: 0.25,
                    completion: {(_: Bool) -> Void in
                        if self.descriptionHeight == 40 {
                            UtilityMethods().hintMe(
                                targetView: self,
                                constraint: self.titleDescriptionHeight,
                                hintHeight: 45.0)
                        }
                    })
            })
    }
    //swiftlint:disable next: function_body_length
    @IBAction func panImage(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: self.view)
        let rollbackY: CGFloat = self.imageCenterY.constant
        self.imageCenterX.constant += translation.x
        if self.imageView.frame.height != self.originalHeight {
            self.imageCenterY.constant += translation.y
        } else {
            self.imageCenterY.constant = self.originY
        }
        self.view.layoutIfNeeded()
        if self.imageView.frame.size.height > self.bottomNavView.frame.origin.y {
            if self.imageView.frame.origin.y > 31.0 {
                self.imageView.frame.origin.y = 31.0
                self.imageCenterY.constant = rollbackY
            }
            let bottomBoundary: CGFloat = self.bottomNavView.frame.origin.y - (self.imageView.frame.height + 1)
            if self.imageView.frame.origin.y < bottomBoundary {
                self.imageView.frame.origin.y = bottomBoundary
                self.imageCenterY.constant = rollbackY
            }
        } else {
            if self.imageView.frame.origin.y < 31.0 {
                self.imageView.frame.origin.y = 31.0
                self.imageCenterY.constant = rollbackY
            }
            let bottomBoundary: CGFloat = self.bottomNavView.frame.origin.y - (self.imageView.frame.height + 1)
            if self.imageView.frame.origin.y > bottomBoundary {
                self.imageView.frame.origin.y = bottomBoundary
                self.imageCenterY.constant = rollbackY
            }
        }
        if self.imageView.frame.origin.x >= self.swipeThreshold  && sender.state == .ended {
            self.swipingImage = true
            self.navVC.navDirection = .previousImage
            NotificationCenter.default.post(
                name: Notification.Name.NotificationType.loadPreviousImage,
                object: nil,
                userInfo: nil)
            return
        }
        let rightBoundary: CGFloat = self.view.frame.width - self.imageView.frame.width
        if self.imageView.frame.origin.x <= rightBoundary - self.swipeThreshold && sender.state == .ended {
            self.swipingImage = true
            self.navVC.navDirection = .nextImage
            NotificationCenter.default.post(
                name: Notification.Name.NotificationType.loadNextImage,
                object: nil,
                userInfo: nil)
            return
        }
        if self.imageView.frame.origin.x >= 1.0  && sender.state == .ended {
            self.imageCenterX.constant = (self.imageView.frame.width - self.view.frame.width)/2
            UIView.animate(withDuration: 0.2, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                self.view.layoutIfNeeded()
            })
        }
        if self.imageView.frame.origin.x < rightBoundary - 1.0 && sender.state == .ended {
            self.imageCenterX.constant = 0 - (self.imageView.frame.width - self.view.frame.width)/2
            UIView.animate(withDuration: 0.2, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                self.view.layoutIfNeeded()
            })
        }
        sender.setTranslation(CGPoint.zero, in: self.view)
    }

    @IBAction func scaleImage(_ sender: UIPinchGestureRecognizer) {
        var imageScale: CGFloat = sender.scale
        if imageScale < self.view.frame.size.width/self.originalWidth {
            imageScale = self.view.frame.size.width/self.originalWidth
        } else {
            self.imageWidth.constant = self.originalWidth * imageScale
            self.imageHeight.constant = self.originalHeight * imageScale
        }
        self.view.layoutIfNeeded()
        self.fillImageMode = true
        if self.imageView.frame.origin.y > 31.0 && self.imageView.frame.size.height > self.originalHeight {
            self.imageCenterY.constant = self.originY
        }
        if self.imageView.frame.origin.x > 20.0 {
            self.imageCenterX.constant =  5.0
        }
        if self.imageView.frame.origin.y < self.view.frame.size.height - self.imageView.frame.height {
            self.imageCenterY.constant = self.view.frame.size.height - self.imageView.frame.height
        }
        if self.imageView.frame.origin.x < self.view.frame.size.width - self.imageView.frame.width {
            self.imageCenterX.constant = self.view.frame.size.width - self.imageView.frame.width
        }
        if self.imageView.frame.origin.y < (self.bottomNavView.frame.origin.y - self.imageView.frame.height)
            && self.imageView.frame.height < self.bottomNavView.frame.origin.y {
            self.imageCenterY.constant = self.originY
        }
        if self.imageView.frame.origin.x < (self.view.frame.width - self.imageView.frame.width)
            && self.imageView.frame.width < self.view.frame.width {
            self.imageCenterX.constant = self.view.frame.width - self.imageView.frame.width
        }
        // make sure less than 100 scale stays centered
        if self.imageView.frame.size.height < self.view.frame.size.height {
            self.imageCenterY.constant = self.originY
            self.imageCenterX.constant = 0
        }
    }

    func fitImage() {
        self.view.layoutIfNeeded()
        self.imageWidth.constant = self.view.frame.size.width
        let imageScale = self.view.frame.width / self.originalWidth
        self.imageHeight.constant = self.imageWidth.constant * imageScale
        self.imageCenterX.constant = 0
        self.imageCenterY.constant = self.originY
        self.tempImageWidth.constant = self.view.frame.size.width
        self.fillImageMode = false
        self.view.layoutIfNeeded()
        UtilityMethods().resizeImageView(
            containerView: self.imageView,
            newWidth: self.view.frame.size.width,
            newHeight: self.originalHeight * imageScale)

    }

    func fillImage() {
        self.imageHeight.constant = self.originalHeight
        self.imageWidth.constant = self.originalWidth
        self.imageCenterX.constant = 0
        self.imageCenterY.constant = self.originY
        self.fillImageMode = true
        self.view.layoutIfNeeded()
        UtilityMethods().resizeImageView(
            containerView: self.imageView,
            newWidth: self.originalWidth,
            newHeight: self.originalHeight)
    }

    func matchImageDimensions() {
        self.view.layoutIfNeeded()
        self.tempImageHeight.constant = self.imageView.frame.size.height
        self.tempImageWidth.constant = self.imageView.frame.size.width
        self.tempImageCenterY.constant = self.imageCenterY.constant
        self.tempImageCenterX.constant = self.imageCenterX.constant
        UtilityMethods().resizeImageView(
            containerView: self.tempImageView,
            newWidth: self.imageWidth.constant,
            newHeight: self.imageHeight.constant)
        self.view.layoutIfNeeded()
    }

    @IBAction func resetImageSize(_ sender: UITapGestureRecognizer) {
        if self.fillImageMode {
            self.fitImage()
        } else {
            self.fillImage()
        }
    }

    @IBAction func showHideControls(_ sender: UITapGestureRecognizer) {
        if self.hideControls {
            self.hideControls = false
        } else {
            self.hideControls = true
        }
        self.titleDescriptionView.isHidden = self.hideControls
        self.exitButton.isHidden = self.hideControls
        self.editButton.isHidden = self.hideControls
        self.refreshTitleDescriptionView()
    }

    func refreshTitleDescriptionView() {
        self.exitButton.isHidden = self.hideControls
        self.editButton.isHidden = self.hideControls
        self.setDescriptionState()
    }

    @objc func setOrigin() {
        let imageHeight: CGFloat = self.imageView.frame.size.height
        let bottomMenuHeight: CGFloat = self.bottomNavView.frame.size.height
        let bottomYorigin: CGFloat = self.bottomNavView.frame.origin.y
        let yOffset = ((bottomYorigin - 35) - imageHeight)/2
        self.originY = 35 - bottomMenuHeight - ((bottomYorigin - imageHeight)/2)
        self.originY += yOffset
        self.view.layoutIfNeeded()
        self.imageCenterX.constant = 0
        self.imageCenterY.constant = self.originY
        if self.tempImage != nil {
            self.tempImageCenterX.constant = self.directionX
            self.tempImageCenterY.constant = self.originY
        }
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.imageView.alpha = 1.0
            self.tempImageView.alpha = 0.0
            self.view.layoutIfNeeded()
        }, completion: {(_: Bool) -> Void in
            self.tempImageView.isHidden = true
            self.tempImage = nil
            self.tempImage = self.presenter.getImage()
            self.tempImageView.contentMode = .scaleAspectFit
            self.tempImageView.image = self.tempImage
            self.tempImageCenterX.constant = 0
            self.tempImageCenterY.constant = self.originY
            self.view.layoutIfNeeded()
            UtilityMethods().resizeImageView(
                containerView: self.tempImageView,
                newWidth: self.originalWidth,
                newHeight: self.originalHeight)
            self.swipingImage = false
            self.refreshTitleDescriptionView()
        })
    }

    @objc func respondToSwipe(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.down:
                self.setImage(image: self.imageView.image!)
                self.dismiss(animated: true, completion: nil)
            default: break
            }
        }
    }

    @IBAction func closeButtonTapped(_ sender: UIButton) {
        setImage( image: self.imageView.image!)
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func editButtonTapped(_ sender: UIButton) {
        present(EditImageDetailsViewController(presenter: EditImageDetailsPresenter(imageObject: presenter.imageAsset)),
                animated: true,
                completion: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self,
                                               name: NSNotification.Name.NotificationType.selectedAssetChanged,
                                               object: nil)
    }

}

class ImageModalPresenter {
    // MARK: Properties
    var imageAsset: PHAsset!
    var view: UIView!
    var imageRecord: ImageDetail!

    init(imageObject: PHAsset) {
        self.imageAsset = imageObject
        self.imageRecord = ImageDetail.getRecordByID(
            identifier: self.imageAsset.localIdentifier,
            context: UtilityMethods().getAppDelegate().persistentContainer.viewContext)
    }

    func setImage(imageObject: PHAsset) {
        self.imageAsset = imageObject
        self.imageRecord = ImageDetail.getRecordByID(
            identifier: self.imageAsset.localIdentifier,
            context: UtilityMethods().getAppDelegate().persistentContainer.viewContext)
    }

    func getImage() -> UIImage {
        return self.imageAsset.getUIFullScaleImage()
    }
    //swiftlint:disable next: file_length
}
